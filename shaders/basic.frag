#version 330 core

uniform vec3 lightColor;
uniform vec3 objectColor;

uniform vec3 lightPos;
uniform vec3 viewPos;

in vec3 FragPos;
in vec3 Normal; 
in vec2 TexCoords; 

out vec4 color;

uniform sampler2D textureDiffuse0;


float near = 0.01; 
float far  = 100.0; 


void main() {
    // Ambient
    float ambientStrength = 0.0f;
    vec3 ambient = ambientStrength * lightColor;    
    
    // Diffuse 
    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(lightPos - FragPos);
    float diff = max(dot(norm, lightDir), 0.0f);
    vec3 diffuse = diff * lightColor;
    
    // Specular
    float specularStrength = 0.5f;
    vec3 viewDir = normalize(viewPos - FragPos);
    vec3 reflectDir = reflect(-lightDir, norm);  
    float spec = pow(max(dot(viewDir, reflectDir), 0.0f), 32.0f);
    vec3 specular = specularStrength * spec * lightColor; 

    float gamma = 2.2;
    //vec3 diffuseColor = texture(textureDiffuse0, TexCoords).rgb;
    vec3 diffuseColor = pow(texture(textureDiffuse0, TexCoords).rgb, vec3(gamma));
    vec3 result = (ambient + diffuse + specular) * diffuseColor;
    color = vec4(result, 1.0f);
    color.rgb = pow(color.rgb, vec3(1.0/gamma));
}