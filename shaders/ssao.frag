#version 330 core

in vec2 fTexCoords;

uniform sampler2D gPositionDepth;
uniform sampler2D gNormal;
uniform sampler2D texNoise;

// SSAO samples
uniform vec3 samples[64];
// projection matrix
uniform mat4 projection;

// final SSAO coerf
out vec4 fColor;

// SSAO constants
int kernelSize = 64;
float radius = 1.0;

const vec2 noiseScale = vec2(800.0 / 4.0, 600.0 / 4.0); 

void main() {

    vec3 fPos = texture(gPositionDepth, fTexCoords).xyz;
    vec3 normal = normalize(texture(gNormal, fTexCoords).xyz);
    vec3 randomVec = texture(texNoise, fTexCoords * noiseScale).xyz;

    // Calculating TBM
    vec3 tangent = normalize(randomVec - normal * dot(randomVec, normal));
    vec3 bitangent = normalize(cross(normal, tangent));
    mat3 TBN = mat3(tangent, bitangent, normal);

    // Calculating oclusion
    float occlusion = 0.0;
    for (int i = 0; i < kernelSize; ++i) {
        
        vec3 sample = TBN * samples[i];
        sample = fPos + sample * radius; 
        
        vec4 offset = vec4(sample, 1.0);
        offset = projection * offset;
        offset.xyz /= offset.w;
        offset.xyz = offset.xyz * 0.5 + 0.5;
        
        float sampleDepth = -texture(gPositionDepth, offset.xy).w;

        if (sampleDepth !=-1.0) {
           float rangeCheck = smoothstep(0.0, 1.0, radius / abs(fPos.z - sampleDepth));
           occlusion += (sampleDepth > sample.z ? 1.0 : 0.0) * rangeCheck;
        }
    }

    occlusion = 1.0 - (occlusion / kernelSize);
    
    fColor = vec4(vec3(occlusion), 1.0);
}