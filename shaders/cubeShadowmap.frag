#version 330 core
in vec4 fPos;

uniform vec3 lightPos;
uniform float farPlane;

void main() {
    float lightDistance = length(fPos.xyz - lightPos);
    lightDistance = lightDistance / farPlane;
    gl_FragDepth = lightDistance;
}  