#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoords;
layout (location = 3) in vec3 tangent;
layout (location = 4) in vec3 bitangent;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out vec3 fPos;
out vec2 fTexCoords;
out vec3 fNormal;
out mat3 tbn; 

void main() {
    gl_Position = projection * view * model * vec4(position, 1.0f);
    fPos        = vec3(model * vec4(position, 1.0f));
    fTexCoords  = texCoords;

    mat3 normalMat  = mat3(transpose(inverse(model)));
    mat3 tangentMat = mat3( model);

    vec3 t = normalize(tangentMat * tangent);
    vec3 b = normalize(tangentMat * bitangent);
    vec3 n = normalize(tangentMat * normal);

    tbn = mat3(t, b, n);

    fNormal = normalize(normalMat * normal); 
}
