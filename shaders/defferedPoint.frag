#version 330 core

// gBuffer texCoords
in vec2 fTexCoords;

// gBuffer
uniform sampler2D gPosition;
uniform sampler2D gNormal;
uniform sampler2D gAlbedo;
uniform sampler2D gSpec;

// shadow
uniform mat4 inversedView;
uniform vec3[24] shadowSamples;
uniform samplerCube depthMap;

// camera
uniform vec3 cameraPos;

// light
uniform vec3  position;
uniform vec3  diffuse;
uniform float intensity;
uniform float constant;
uniform float linear;
uniform float quadratic;

// material
uniform float shininess;

// result color
out vec4 fColor;


float shadowCalculation(vec3 fPos) {
    vec3  fragToLight  = fPos - position;
    float bias         = 0.25;
    float shadow       = 0.0;
    float diskRadius   = 0.05;
    
    for(int i = 0; i < 24; ++i) {
        float closestDepth = texture(depthMap, mat3(inversedView) * (fragToLight + shadowSamples[i] * diskRadius)).r;
        closestDepth      *= 30.0f;//farPlane;

        float currentDepth = length(fragToLight);
        shadow            += currentDepth - bias > closestDepth ? 1.0 : 0.0;
    }

    return 1.0 - shadow / 24.0;
}

void main() {
    vec3  fPos       = texture(gPosition, fTexCoords).xyz;
    vec3  fNormal    = texture(gNormal, fTexCoords).xyz;
    vec3  fDiffuse   = texture(gAlbedo, fTexCoords).rgb;
    vec3  fSpecular  = texture(gSpec, fTexCoords).rgb;

    vec3  viewDir    = normalize(cameraPos - fPos);

    vec3  lightR     = position - fPos;
    vec3  lightDir   = normalize(lightR);
    float dist       = length(lightR);
    float atenuation = constant + linear * dist + quadratic * dist * dist; 
    float diff       = max(dot(fNormal, lightDir), 0.0) / atenuation;

    vec3  reflectDir = normalize(reflect(-lightDir, fNormal));
    float spec       = pow(max(dot(viewDir, reflectDir), 0.0), shininess) / atenuation;
    
    vec3  diffuse    = diffuse * intensity * diff * fDiffuse;
    vec3  specular   = diffuse * intensity * spec * fSpecular;

    float shadow     = shadowCalculation(fPos);

    fColor           = vec4((diffuse + specular) * shadow, 1.0);      
}