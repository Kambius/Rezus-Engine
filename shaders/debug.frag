#version 330 core

uniform sampler2D screenTexture;

in vec2 TexCoords;

out vec4 color;

void main() {
    color = vec4(clamp(texture(screenTexture, TexCoords).rgb, 0.0, 1.0), 1.0);
}  