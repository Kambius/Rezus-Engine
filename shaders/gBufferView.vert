#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoords;
layout (location = 3) in vec3 tangent;
layout (location = 4) in vec3 bitangent;

uniform mat4 model;      // model matrix
uniform mat4 view;       // view matrix
uniform mat4 projection; // projection matrix

out vec3 fPos;        // vertex position
out vec3 fNormal;     // vertex normal
out vec2 fTexCoords;  // texture coords 
out mat3 tbn;         // TBN matrix

void main() {
    // projects vertex 
    gl_Position = projection * view * model * vec4(position, 1.0f);
    // sets vertex position
    fPos        = vec3(view * model * vec4(position, 1.0f));
    // sets vertex texture coords
    fTexCoords  = texCoords;

    // calculates normal and tangent matrix
    mat3 normalMat  = mat3(transpose(inverse(view * model)));
    mat3 tangentMat = mat3(view * model);

    vec3 t = normalize(tangentMat * tangent);
    vec3 b = normalize(tangentMat * bitangent);
    vec3 n = normalize(tangentMat * normal);

    tbn = mat3(t, b, n);

    fNormal = normalize(normalMat * normal);
}
