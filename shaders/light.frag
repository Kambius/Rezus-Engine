#version 330 core

struct Material {
    vec3      ambient;
    vec3      diffuse;
    vec3      specular;
    float     shininess;
    sampler2D diffuseTexture;
}; 

struct AmbientLight {
    vec3  diffuse;
    float intensity;
};


struct DirectionalLight {
    vec3  direction;
    vec3  diffuse;
    float intensity;
};

struct PointLight {
    vec3  position;
    vec3  diffuse;
    float intensity;
    float constant;
    float linear;
    float quadratic;
};

in vec3 fPos;
in vec3 fNormal; 
in vec2 fTexCoords; 

uniform Material material;
uniform vec3     cameraPos;

uniform AmbientLight        ambientLight;
uniform DirectionalLight    directionalLight;
uniform PointLight          pointLight;

out vec4 color;

vec3 calcAmbientLight(AmbientLight light, Material material)
{
    return light.diffuse * light.intensity * material.ambient;
}

vec3 calcDirectionalLight(DirectionalLight light, vec3 normal, vec3 viewDir, Material material)
{
    vec3 lightDir   = normalize(-light.direction);
    
    // Diffuse shading
    float diff      = max(dot(normal, lightDir), 0.0);
    
    // Specular shading
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec      = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    
    // Combine results
    vec3 diffuse    = light.diffuse * light.intensity * diff * material.diffuse * texture(material.diffuseTexture, fTexCoords).rgb;
    vec3 specular   = light.diffuse * light.intensity * spec * material.specular;
    
    return diffuse + specular; 
}

vec3 calcPointLight(PointLight light, vec3 normal, vec3 viewDir, Material material)
{
    vec3 lightDir   = normalize(light.position - fPos);

    // Diffuse shading
    float diff      = max(dot(normal, lightDir), 0.0);
    
    // Specular shading
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec      = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    
    // Attenuation
    float distance    = length(light.position - fPos);
    float attenuation = 1.0 / (light.constant + light.linear * distance + 
                 light.quadratic * (distance * distance));    
    
    // Combine results
    vec3 diffuse  = light.diffuse * light.intensity * diff * material.diffuse * texture(material.diffuseTexture, fTexCoords).rgb;
    vec3 specular = light.diffuse * light.intensity * spec * material.specular;
    
    return (diffuse + specular) * attenuation;
}

vec3 calcTotalLight(vec3 normal, vec3 viewDir) 
{
    vec3 total = vec3(0.0);
    
    total += calcAmbientLight(ambientLight, material);
    total += calcDirectionalLight(directionalLight, normal, viewDir, material);
    total += calcPointLight(pointLight, normal, viewDir, material);
    
    return total;
}


void main()
{
    vec3 normal = normalize(fNormal);
    vec3 viewDir = normalize(cameraPos - fPos);

    vec3 light = calcTotalLight(normal, viewDir);

    color = vec4(light, 1.0);
}

