#version 330 core

uniform vec3 lightColor;
uniform vec3 objectColor;

uniform vec3 lightPos;
uniform vec3 viewPos;

in vec3 FragPos;
in vec3 Normal; 
in vec2 TexCoords; 

out vec4 color;

uniform sampler2D textureDiffuse0;

void main() { 
    color = vec4(vec3(gl_FragCoord.z), 1.0f);;
    //color = vec4(0.0f, 1.0f, 1.0f, 1.0f);;
}