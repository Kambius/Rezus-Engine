#version 330 core

// gBuffer texCoords
in vec2 fTexCoords;

// gBuffer
uniform sampler2D gAlbedo;
uniform sampler2D ssaoMask;

// light
uniform vec3  diffuse;
uniform float intensity;

// result color
out vec4 fColor;

void main() {
    vec3 fDiffuse = texture(gAlbedo, fTexCoords).rgb;
    vec3 mask     = texture(ssaoMask, fTexCoords).rgb;
    vec3 ambient  = fDiffuse * diffuse * intensity * mask;  
    fColor        = vec4(ambient, 1.0); 
    //fColor        = vec4(fDiffuse, 1.0); 
}