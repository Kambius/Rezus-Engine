#version 330 core

// gBuffer texCoords
in vec2 fTexCoords;

// gBuffer
uniform sampler2D gPosition;
uniform sampler2D gNormal;
uniform sampler2D gAlbedo;
uniform sampler2D gSpec;

// camera
uniform vec3 cameraPos;

// light
uniform vec3  direction;
uniform vec3  diffuse;
uniform float intensity;

// shadow
uniform sampler2D shadowMap;
uniform mat4 shadowMat;

// material
uniform float shininess;

// result color
out vec4 fColor;

const int shadowSmooth = 2;

float shadowCalculation(vec4 fragPosLightSpace) {
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    projCoords = projCoords * 0.5 + 0.5;
    float bias = 0.01;
    float currentDepth = projCoords.z;

    float shadow = 0.0;
    vec2 texelSize = 1.0 / textureSize(shadowMap, 0);

    for (int x = -shadowSmooth; x <= shadowSmooth; ++x) {
        for (int y = -shadowSmooth; y <= shadowSmooth; ++y) {
            float pcfDepth = texture(shadowMap, projCoords.xy + vec2(x, y) * texelSize).r;
            shadow += currentDepth - bias > pcfDepth ? 0.0 : 1.0;
        }
    }

    shadow /= (shadowSmooth * 2 + 1) * (shadowSmooth * 2 + 1);
   
    if (projCoords.z > 1.0) shadow = 1.0;
    return shadow;
}  

void main() {
    vec3  fPos       = texture(gPosition, fTexCoords).xyz;
    vec3  fNormal    = texture(gNormal, fTexCoords).xyz;
    vec3  fDiffuse   = texture(gAlbedo, fTexCoords).rgb;
    vec3  fSpecular  = texture(gSpec, fTexCoords).rgb;

    vec3  viewDir    = normalize(cameraPos-fPos);

    vec3  lightDir   = normalize(-direction);
    float diff       = max(dot(fNormal, lightDir), 0.0);

    vec3  reflectDir = normalize(reflect(direction, fNormal));
    float spec       = pow(max(dot(viewDir, reflectDir), 0.0), shininess);

    vec3  diffuse    = diffuse * intensity * diff * fDiffuse;
    vec3  specular   = diffuse * intensity * spec * fSpecular;

    float shadow     = shadowCalculation(shadowMat * vec4(fPos, 1.0));

    fColor           = vec4(shadow * (diffuse + specular), 1.0);                
}