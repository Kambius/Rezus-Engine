#version 330 core

struct Material {
    vec3      ambient;
    vec3      diffuse;
    vec3      specular;
    float     shininess;
    sampler2D diffuseTexture;
    sampler2D specularTexture;
    sampler2D normalTexture;
}; 

in vec3 fPos;
in vec3 fNormal;
in vec2 fTexCoords; 
in mat3 tbn;

uniform Material material;

layout (location = 0) out vec4 gPosition;
layout (location = 1) out vec3 gNormal;
layout (location = 2) out vec3 gAlbedo;
layout (location = 3) out vec3 gSpec;

const float NEAR = 0.1; 
const float FAR = 100.0;

float linearizeDepth(float depth) {
    float z = depth * 2.0 - 1.0; 
    return (2.0 * NEAR * FAR) / (FAR + NEAR - z * (FAR - NEAR));    
}

void main() {
    gPosition       = vec4(fPos, linearizeDepth(gl_FragCoord.z));

    gNormal         = texture(material.normalTexture, fTexCoords).xyz;
    gNormal         = gNormal * 2.0 - 1.0;
    gNormal         = normalize(tbn * gNormal);

    //gNormal         = normalize(fNormal);

    gAlbedo         = texture(material.diffuseTexture, fTexCoords).rgb * material.diffuse;
    gSpec           = texture(material.specularTexture, fTexCoords).rgb * material.specular;
}
