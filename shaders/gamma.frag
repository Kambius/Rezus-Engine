#version 330 core

uniform sampler2D screenTexture;
uniform float gamma;

in vec2 TexCoords;

out vec4 color;

void main() {
   vec3 original = texture(screenTexture, TexCoords).rgb;
   vec3 corrected = pow(original, vec3(1.0 / gamma));
   color = vec4(corrected, 1.0);
}  