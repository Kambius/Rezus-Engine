#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <string>

#define CURSOR_NORMAL     GLFW_CURSOR_NORMAL
#define CURSOR_DISABLED   GLFW_CURSOR_DISABLED
#define CURSOR_HIDDEN     GLFW_CURSOR_HIDDEN

class Input;

class Window {
    GLFWwindow* window;
    Input* input;    
    int width, height;
public:
    Window(std::string title, int width, int height);
    ~Window();  
    
    bool shouldClose();
    void updateInput();
    void swapBuffers();

    Input* getInput();
    
    void setCursorMode(int mode);

    int getWidth();
    int getHeight();
private:
    static void errorCallback(int error, const char* description);
    static void framebufferSizeCallback(GLFWwindow* window, int w, int h);
    static void keyCallback(GLFWwindow* window, int key, int /*scancode*/, int action, int /*mods*/);
    static void mouseButtonCallback(GLFWwindow* window, int button, int action, int /*mods*/);
    static void cursorPositionCallback(GLFWwindow* window, double xpos, double ypos);
};