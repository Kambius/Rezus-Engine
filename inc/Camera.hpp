#pragma once

#include <glm/glm.hpp>
#include <glm/ext.hpp>

class Camera {
    glm::vec3 position;
    glm::vec3 up;
    glm::vec3 front;
public:
    Camera();
    Camera(glm::vec3 pos, glm::vec3 up, glm::vec3 front);
    
    glm::mat4 getViewMatrix();
    glm::mat4 getProjectionMatrix();
    
    glm::vec3 getPosition();
    glm::vec3 getUp();
    glm::vec3 getFront();
    float getPitch();
    float getYaw();

    void setPosition(glm::vec3 pos);
    void setUp(glm::vec3 up);
    void setFront(glm::vec3 front);
    void setPitch(float p);
    void setYaw(float y);

    void moveForward(float amount);
    void moveBack(float amount);
    void moveUp(float amount);
    void moveDown(float amount);
    void moveLeft(float amount);
    void moveRight(float amount);
    void lookAt(glm::vec3 point);
};