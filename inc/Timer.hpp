#pragma once

class Timer {
    double startTime;
    double time;
public:
    Timer();
    void reset();
    int stop();
    int getTime();
};
