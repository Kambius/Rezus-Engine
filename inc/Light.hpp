#pragma once

#include <glm/glm.hpp>

struct AmbientLight {
    glm::vec3 diffuse;
    float     intensity;

    AmbientLight() 
        : diffuse   { 1.0f, 1.0f, 1.0f }
        , intensity { 0.1f } { } 
};

struct DirectionalLight {
    glm::vec3 direction;
    glm::vec3 diffuse;
    float     intensity;

    DirectionalLight()
        : direction { 0.5f,-1.0f, 0.4f }
        , diffuse   { 1.0f, 1.0f, 1.0f }
        , intensity { 0.2f } { } 
};

struct PointLight {
    glm::vec3 position;
    glm::vec3 diffuse;
    float     intensity;
    float     constant;
    float     linear;
    float     quadratic;

    PointLight()
        : position  { 0.0f, 0.0f, 0.0f }
        , diffuse   { 1.0f, 1.0f, 1.0f }
        , intensity { 1.0f }
        , constant  { 0.0f }
        , linear    { 0.0f }
        , quadratic { 1.0f } { }
};