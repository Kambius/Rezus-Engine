#pragma once

#include "CubeMap.hpp"
#include <GL/glew.h>

class DepthCubeBuffer {
    int width;
    int height;
    GLuint depthCubeBuffer;
    CubeMap* depthCubeMap;
public:
    DepthCubeBuffer(int width, int height);
    void bind();
    int getWidth();
    int getHeight();
    CubeMap* getDepthCubeMap();
};
