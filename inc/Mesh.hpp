#pragma once

#include "Shader.hpp"
#include "Material.hpp"
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <vector>

struct Vertex {
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec2 texCoords;
    glm::vec3 tangent;
    glm::vec3 bitangent;
};

class Mesh {
public:
    std::vector<Vertex> vertices;
    std::vector<GLuint> indices;
    
    Mesh(std::vector<Vertex> vertices, std::vector<GLuint> indices);
    void draw();
private:
    GLuint VAO, VBO, EBO;
    void setupMesh();
};