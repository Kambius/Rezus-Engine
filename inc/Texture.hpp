#pragma once

#include <string>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

class Texture {
public:
    Texture(std::string filename);
    Texture(int width, int height, GLint internalFormat, GLint externalFormat, GLint type, void* data);
    
    void dispose();
    void bind();
    int getWidth();
    int getHeight();
    GLuint getID();
    std::string getPath();
  
    void setParameter(GLint param, GLint value);

    void setBorderColor(GLfloat r, GLfloat g, GLfloat b, GLfloat a);

    static Texture* nullTexture();
    static Texture* flatNormalTexture();
private:
    static Texture nTexture;
    static Texture fNormalTexture;

    Texture();

    GLuint textureID;
    int width;
    int height;
    std::string path; 
};
