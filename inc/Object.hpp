#pragma once

#include "Model.hpp"
#include "Transform.hpp"
#include <vector>
#include <string>

// Scene object
class Object {
public:
    // List of meshes
    std::vector<Mesh*> meshes;
    // List of materials
    std::vector<Material*> materials;
    // Onject tramsformations
    Transform* transform;
    Object();
    Object(std::string filename);
};
