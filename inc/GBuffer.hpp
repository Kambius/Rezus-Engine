#pragma once

#include "Texture.hpp"
#include <GL/glew.h>

class GBuffer {
    int width;
    int height;
    GLuint gBuffer;
    Texture* gPosition;
    Texture* gNormal;
    Texture* gAlbedo;
    Texture* gSpec;
public:
    GBuffer(int width, int height);
    void bind();
    int getWidth();
    int getHeight();
    Texture* getPositionTexture();
    Texture* getNormalTexture();
    Texture* getAlbedoTexture();
    Texture* getSpecTexture();
};
