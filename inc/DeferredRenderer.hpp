#pragma once

#include "Transform.hpp"
#include "Model.hpp"
#include "Shader.hpp"
#include "Camera.hpp"
#include "Light.hpp"
#include "FrameBuffer.hpp"
#include "GBuffer.hpp"
#include "DepthBuffer.hpp"
#include "DepthCubeBuffer.hpp"
#include "Texture.hpp"
#include "Object.hpp"
#include "Plane.hpp"
#include "Cube.hpp"
#include <vector>

class DeferredRenderer {
    // ilumination constants
    static constexpr int SHADOW_WIDTH = 1024;
    static constexpr int SHADOW_HEIGHT = 1024;
    static constexpr int SSAO_NOISE_SIZE = 4;
    static constexpr int SSAO_SAMPLES = 64;
    static constexpr GLfloat SSAO_RADIUS = 1.0;

    // Offscreen buffers
    GBuffer*     gBuffer;
    FrameBuffer* ssaoBuffer;
    DepthBuffer* directionalShadowBuffer;
    DepthCubeBuffer* pointShadowBuffer;
    FrameBuffer* postprocBuffer;

    // Shaders
    Shader* gBufferShader;
    Shader* ssaoShader;
    Shader* shadowShader;
    Shader* cubeShadowShader;
    Shader* ambientShader;
    Shader* directionalShader;
    Shader* pointShader;
    Shader* gammaShader;
    Shader* debugShader;

    // ssao noise
    Texture* ssaoNoiseTexture;

    std::vector<glm::vec3> ssaoKernel;

    // Plane for post processing
    Plane* screenPlane;

    // Dimentions of draw area
    int width;
    int height;
    
    // Scene components 
    Camera*                        camera;
    std::vector<Object*>           objects;
    std::vector<AmbientLight*>     ambientLights;
    std::vector<DirectionalLight*> directionalLights;
    std::vector<PointLight*>       pointLights;

    // Renders scene to G-Buffer
    void fillGBuffer();
    // Calcrulates SSAO mask
    void calculateSSAO();
    // Creates shadow map
    glm::mat4 calculateDirrectionalShadow(DirectionalLight* l);
    // Creates cube shadow map
    void calculatePointShadow(PointLight* l);
    // Renders ambient lights
    void drawAmbientLights();
    // Renders directional lights
    void drawDirectionalLights();
    // Renders point lights
    void drawPointLights();
    // Renders csene to screen and do gamma corection
    void doGammaCorrection();
public:
    // Gamma value
    float gamma = 1.7f;

    DeferredRenderer(int width, int height);

    // Sets main camera
    void setCamera(Camera* c);
    // Adds object to scene
    void addObject(Object* o);
    // Adds AmbientLight to scene
    void addAmbientLight(AmbientLight* l);
    // Adds DirectionalLight to scene
    void addDirectionalLight(DirectionalLight* l);
    // Adds PointLight to scene
    void addPointLight(PointLight* l);
    // Removes object to scene
    void removeObject(Object* o);
    // Removes AmbientLight to scene
    void removeAmbientLight(AmbientLight* l);
    // Removes DirectionalLight to scene
    void removeDirectionalLight(DirectionalLight* l);
    // Removes PointLight to scene
    void removePointLight(PointLight* l);

    // Draws scene in normal mode 
    void draw();

    // Debug funcrion, draws SSAO mask
    void drawSSAOMask();
    // Debug funcrion, draws normal tecture
    void drawNormals();
    // Debug funcrion, draws color texture
    void drawColorMask();
    // Debug funcrion, draws specular texture
    void drawSpecMask();
    // Debug funcrion, draws position texture
    void drawPositions();
    // Debug funcrion, draws scene only with ambuent lights
    void drawAmbientOnly();
    // Debug funcrion, draws scene only with directional lights
    void drawDirectionalOnly();
    // Debug funcrion, draws scene only with point lights
    void drawPointOnly();
};
