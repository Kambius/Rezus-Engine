#pragma once 

#include "Mesh.hpp"
#include <vector>
#include <string>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

class Model {
public:
    Model(std::string filename);
    void draw(Shader* shader);
    void drawAllMeshes();
    std::vector<Mesh*> meshes;
    std::vector<Material*> materials;    
private:
    std::string directory;
    void loadModel(std::string path);
    void processNode(aiNode* node, const aiScene* scene);
    void processMesh(aiMesh* mesh, const aiScene* scene);
    std::vector<Texture*> loadMaterialTextures(aiMaterial* mat, aiTextureType type);  
};
