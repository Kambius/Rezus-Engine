#pragma once

#include "Window.hpp"
#include "Input.hpp"

using Func = void (*)();

class Engine {
    static bool running;
    
    static Window* window;
    static Input* input;

    static Func update; 
    static Func draw;

    static void mainLoop();
    static void terminate();
public:
    static void init();
    static void start();
    static void stop();

    static void setUpdate(Func update);
    static void setDraw(Func draw);

    static Window& getWindow();
    static Input& getInput();
};