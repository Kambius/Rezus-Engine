#pragma once

#include "Material.hpp"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <map>
#include <string>

class Shader {
public:
    Shader(std::string vertexFilePath, std::string fragmentFilePath);
    Shader(std::string vertexFilePath, std::string geometryFilePath, std::string fragmentFilePath);

    void bind();
    void dispose();

    void setUniform(std::string uName, GLfloat v0);
    void setUniform(std::string uName, GLfloat v0, GLfloat v1);
    void setUniform(std::string uName, GLfloat v0, GLfloat v1, GLfloat v2);
    void setUniform(std::string uName, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3);

    void setUniform(std::string uName, GLint v0);
    void setUniform(std::string uName, GLuint v0);

    void setUniform(std::string uName, glm::vec2 v);
    void setUniform(std::string uName, glm::vec3 v);
    void setUniform(std::string uName, glm::vec4 v);
    void setUniform(std::string uName, glm::mat4 v);

    void setUniform(std::string uName, Material m);
private:
    GLuint shaderID;
    GLuint getUniformLocation(std::string uName);
    GLuint loadShaders(std::string vertexFilePath, std::string geometryFilePath, std::string fragmentFilePath);
    std::map<std::string,GLuint> uniforms;
};


