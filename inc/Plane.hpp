#pragma once

#include "Mesh.hpp"

class Plane : public Mesh {
public:
    Plane(float width, float height);
};
