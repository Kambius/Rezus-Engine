#pragma once

#include "Texture.hpp"
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <vector>

struct Material {
    glm::vec3 ambient;
    glm::vec3 diffuse;
    glm::vec3 specular;
    Texture* diffuseTexture;
    Texture* specularTexture;
    Texture* normalTexture;
    float shininess;
    Material();
};