#pragma once

#include <Python.h>
#include <map>
#include <cstdarg>

namespace rz {
namespace python {
 
class PyError { };

class PyObj {
    PyObject* obj;
    
    friend void finalize();
    static bool finalized;                      
    static std::map<PyObject*, size_t> refs;    

public:

    PyObj();
    PyObj(PyObject* obj);
    ~PyObj();
    
    PyObj(const PyObj& other);
    PyObj(PyObj&& other);

    PyObj& operator=(const PyObj& other);
    PyObj& operator=(PyObj&& other);

    PyObject* operator->();
    PyObject& operator*();

    PyObject* operator->() const;
    PyObject& operator*() const;

    operator PyObject*() const;
    operator bool() const;

    PyObj atr(const char* name);
    PyObj call(const char* format=NULL, ...);
    PyObj callWithArgs(...);
    PyObj method(const char* method, const char* format=NULL, ...);
    PyObj methodWithArgs(const char* method, ...);

    bool isSubclassOf(PyObj base);
    bool isInstanceOf(PyObj what);

    size_t lenght();
};

void init();
void finalize();
PyObj import(const char* module);

void checkError();

template <typename T> 
T convert(PyObj obj) {
    throw PyError { };
}

template <typename T> 
PyObj convert(const char* format, ...) {
    throw PyError { };
}

}}