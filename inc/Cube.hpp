#pragma once

#include "Mesh.hpp"

class Cube : public Mesh {
public:
    Cube(float size);
};
