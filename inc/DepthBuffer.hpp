#pragma once

#include "Texture.hpp"
#include <GL/glew.h>

class DepthBuffer {
    int width;
    int height;
    GLuint depthBuffer;
    Texture* depthTexture;
public:
    DepthBuffer(int width, int height);
    void bind();
    int getWidth();
    int getHeight();
    Texture* getDepthTexture();
};
