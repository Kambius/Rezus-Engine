#pagma once

class RenderingContext {
public:
    RenderingContext();
    ~RenderingContext();
    init();
    setFrameBuffer(FrameBuffer fb);
    setShader(Shader* s);
    setSkyBox(SkyBox sb);
    clearBuffers();
};