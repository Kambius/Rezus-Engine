#pragma once

#include "Camera.hpp"

class FreeCamera : public Camera {
    float speed;
    float lastUpdateTime;
    bool firstUpdate;
    float sensitivity;
public:
    FreeCamera();
    FreeCamera(glm::vec3 pos, glm::vec3 up, glm::vec3 front, float speed, float sensitivity);

    void update();
    
    float getSpeed();
    float getSensitivity();

    void setSpeed(float s);
    void setSensitivity(float s);
};
