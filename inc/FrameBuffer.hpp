#pragma once

#include "Texture.hpp"
#include <GL/glew.h>

class FrameBuffer {
    int width;
    int height;
    GLuint framebuffer;
    Texture* colorTexture;
public:
    FrameBuffer(int width, int height);
    void bind();
    int getWidth();
    int getHeight();
    Texture* getColorTexture();
};
