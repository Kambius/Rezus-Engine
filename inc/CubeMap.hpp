#pragma once

#include <string>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

class CubeMap {
public:
    CubeMap(int width, int height, GLint internalFormat, GLint externalFormat, GLint type, void* data);
    
    void dispose();
    void bind();
    GLuint getID();
  
    void setParameter(GLint param, GLint value);
private:
    GLuint cubeMapID;
};
