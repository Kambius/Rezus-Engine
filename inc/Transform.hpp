#pragma once

#include <glm/glm.hpp>
#include <glm/ext.hpp>

class Transform {
    glm::vec3 tr;
    glm::vec3 sc;
    glm::quat rt;
public:
    Transform();

    glm::mat4 getMatrix();

    Transform& translate(glm::vec3 t);
    Transform& scale(glm::vec3 s);
    Transform& scale(float s);
    Transform& rotate(float angle, glm::vec3 axis);

    Transform& setTranslation(glm::vec3 t);
    Transform& setScale(glm::vec3 s);
    Transform& setScale(float s);
    Transform& setRotation(float angle, glm::vec3 axis);

    glm::vec3 getTranslation();
    glm::vec3 getScale();
    glm::quat getRotation();
    glm::vec3 getRotationAxis();
    float     getRotationAngle();
};