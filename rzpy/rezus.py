class RzObject:
    pos = (0,0,0)
    def init(self): pass
    def update(self): pass

class Player(RzObject):
    def init(self):
        self.pos = (1, 1, 1)

    def update(self):
        self.pos = (self.pos[0], self.pos[1] + 0.2, self.pos[2])
        print(self.pos)
        return self.pos[1]

    def __del__(self):
        print("DESTRUCTOR")