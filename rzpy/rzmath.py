from math import sqrt
from numbers import Number

class Vec2:

    def __init__(self, x=0.0, y=0.0):
        self.x = x
        self.y = y
        
    def dot(self, v):
        return self.x * v.x + self.y * v.y

    def cros(self, v):
        return self.x * v.y - self.y * v.x 

    def length(self):
        return sqrt(self.x ** 2 + self.y ** 2)

    def normalized(self):
        l = self.length()
        return Vec2(x / l, y / l)

    def __add__(self, v):
        return Vec2(self.x + v.x, self.y + v.y)

    def __sub__(self, v):
        return Vec2(self.x - v.x, self.y - v.y)

    def __mul__(self, v):
        if isinstance(v, Vec2):
            return Vec2(self.x * v.x, self.y * v.y)
        else:
            return Vec2(self.x * v, self.y * v)

    def __truediv__(self, v):
        if isinstance(v, Vec2):
            return Vec2(self.x / v.x, self.y / v.y)
        else:
            return Vec2(self.x / v, self.y / v)

    def aslist(self):
        return [self.x, self.y]

    def __str__(self):
        return '(% .2f,% .2f)' % (self.x, self.y)


class Vec3:

    def __init__(self, x=0.0, y=0.0, z=0.0):
        self.x = x
        self.y = y
        self.z = z
        
    def dot(self, v):
        return self.x * v.x + self.y * v.y + self.z * v.z

    def cros(self, v):
        x = self.y * v.z - self.z * v.y 
        y = self.z * v.x - self.x * v.z
        z = self.x * v.y - self.y * v.x
        return Vec3(x, y, z)

    def length(self):
        return sqrt(self.x ** 2 + self.y ** 2 + self.z ** 2)

    def normalized(self):
        l = self.length()
        return Vec3(x / l, y / l, z / l)

    def __add__(self, v):
        return Vec3(self.x + v.x, self.y + v.y, self.z + v.z)

    def __sub__(self, v):
        return Vec3(self.x - v.x, self.y - v.y, self.z - v.z)

    def __mul__(self, v):
        if isinstance(v, Vec3):
            return Vec3(self.x * v.x, self.y * v.y, self.z * v.z)
        else:
            return Vec3(self.x * v, self.y * v, self.z * v)

    def __truediv__(self, v):
        if isinstance(v, Vec3):
            return Vec3(self.x / v.x, self.y / v.y, self.z / v.z)
        else:
            return Vec3(self.x / v, self.y / v, self.z / v)

    def aslist(self):
        return [self.x, self.y, self.z]

    def __str__(self):
        return '(% .2f,% .2f,% .2f)' % (self.x, self.y, self.z)


class Vec4:

    def __init__(self, x=0.0, y=0.0, z=0.0, w=0.0):
        self.x = x
        self.y = y
        self.z = z
        self.w = w
        
    def dot(self, v):
        return self.x * v.x + self.y * v.y + self.z * v.z + self.w * v.w

    def length(self):
        return sqrt(self.x ** 2 + self.y ** 2 + self.z ** 2 + self.w ** 2)

    def normalized(self):
        l = self.length()
        return Vec4(x / l, y / l, z / l, w / l)

    def __add__(self, v):
        return Vec4(self.x + v.x, self.y + v.y, self.z + v.z, self.w + v.w)

    def __sub__(self, v):
        return Vec4(self.x - v.x, self.y - v.y, self.z - v.z, self.w - v.w)

    def __mul__(self, v):
        if isinstance(v, Vec4):
            return Vec4(self.x * v.x, self.y * v.y, self.z * v.z, self.w * v.w)
        else:
            return Vec4(self.x * v, self.y * v, self.z * v, self.w * v)

    def __truediv__(self, v):
        if isinstance(v, Vec4):
            return Vec4(self.x / v.x, self.y / v.y, self.z / v.z, self.w / v.w)
        else:
            return Vec4(self.x / v, self.y / v, self.z / v, self.w / v)

    def aslist(self):
        return [self.x, self.y, self.z, self.w]

    def __str__(self):
        return '(% .2f,% .2f,% .2f,% .2f)' % (self.x, self.y, self.z, self.w)


class Mat4:
    
    def __init__(self, m=1.0):
        if isinstance(m, Number):
            self.m = ([m  , 0.0, 0.0, 0.0],
                      [0.0, m  , 0.0, 0.0],
                      [0.0, 0.0, m  , 0.0],
                      [0.0, 0.0, 0.0, m  ])
        elif isinstance(m, list):
            self.m = (m[0:4],
                      m[4:8],
                      m[8:12],
                      m[12:16])
        elif isinstance(m, tuple):
            self.m = m 

    def __getitem__(self,index):
        return self.m[index]

    def __add__(self, m):
        res = Mat4(0.0)
        for i in range(4):
            for j in range(4):
                res[i][j] = self[i][j] + m[i][j]
        return res

    def __sub__(self, m):
        res = Mat4(0.0)
        for i in range(4):
            for j in range(4):
                res[i][j] = self[i][j] - m[i][j]
        return res

    def __mul__(self, v):
        if isinstance(v, Number):
            res = Mat4(0.0)
            for i in range(4):
                for j in range(4):
                    for k in range(4):
                        res[i][j] = self[i][j] * v
            return res
        elif isinstance(v, Mat4):
            res = Mat4(0.0)
            for i in range(4):
                for j in range(4):
                    for k in range(4):
                        res[i][j] += self[i][k] * v[k][j]
            return res
        elif isinstance(v, Vec4):
            res = Vec4()
            res.x = self[0][0] * v.x + self[0][1] * v.y + self[0][2] * v.z + self[0][3] * v.w
            res.y = self[1][0] * v.x + self[1][1] * v.y + self[1][2] * v.z + self[1][3] * v.w
            res.z = self[2][0] * v.x + self[2][1] * v.y + self[2][2] * v.z + self[2][3] * v.w
            res.w = self[3][0] * v.x + self[3][1] * v.y + self[3][2] * v.z + self[3][3] * v.w
            return res

    def __truediv__(self, m):
        return self * (1.0 / m) 

    def aslist(self):
        return [*m[0], *m[1], *m[2], *m[3]]

    def __str__(self):
        return '(% .2f,% .2f,% .2f,% .2f)\n' * 4 % (*self.m[0], *self.m[1], *self.m[2], *self.m[3])

