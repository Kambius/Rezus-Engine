#include "DepthCubeBuffer.hpp"
#include <glm/glm.hpp>
#include <iostream>

using namespace std;

DepthCubeBuffer::DepthCubeBuffer(int width, int height)
    : width  { width }
    , height { height } {

    depthCubeMap = new CubeMap(width, height, GL_DEPTH_COMPONENT, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);

    glGenFramebuffers(1, &depthCubeBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, depthCubeBuffer);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthCubeMap->getID(), 0);
    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        cout << "ERROR::DEPTHBUFFER:: DepthCubeBuffer is not complete!" << endl;
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void DepthCubeBuffer::bind() {
    glBindFramebuffer(GL_FRAMEBUFFER, depthCubeBuffer);
}

int DepthCubeBuffer::getWidth() {
    return width;
}

int DepthCubeBuffer::getHeight() {
    return height;
}

CubeMap* DepthCubeBuffer::getDepthCubeMap() {
    return depthCubeMap;
}
