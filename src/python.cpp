#include "python.hpp"
#include <iostream>

using namespace std;

namespace rz {
namespace python {
 
bool PyObj::finalized { false };
map<PyObject*, size_t> PyObj::refs { };

PyObj::PyObj()
    : obj { nullptr } { }

PyObj::PyObj(PyObject* obj)
    : obj { obj } { 
    refs[obj]++;
}

PyObj::~PyObj() {
    if (!finalized) {
        Py_XDECREF(obj);
        checkError();
        refs[obj]--;
    }
}

PyObj::PyObj(const PyObj& other) 
    : obj { other.obj } {
    Py_XINCREF(obj);
    checkError();
    refs[obj]++;
}

PyObj::PyObj(PyObj&& other)
    : obj { other.obj } {

    other.obj = nullptr;
}

PyObj& PyObj::operator=(const PyObj& other) {
    
    if (obj != other.obj) {
        Py_XDECREF(obj);
        checkError();
        refs[obj]--;
        
        Py_XINCREF(other.obj);
        checkError();
        refs[other.obj]++;
        
        obj = other.obj;
    }
    
    return *this;    
}

PyObj& PyObj::operator=(PyObj&& other) {
    Py_XDECREF(obj);
    checkError();
    refs[obj]--;
    obj = other.obj;
    other.obj = nullptr;
    return *this;
}

PyObject* PyObj::operator->() {
    return obj;
}

PyObject& PyObj::operator*() {
    return *obj;
}

PyObject* PyObj::operator->() const {
    return obj;
}

PyObject& PyObj::operator*() const {
    return *obj;
}

PyObj::operator PyObject*() const {
    return obj;
}

PyObj::operator bool() const {
    return obj;
}

PyObj PyObj::atr(const char* name) {
    PyObject* res = PyObject_GetAttrString(obj, name);
    checkError();
    return res;
}

PyObj PyObj::call(const char* format, ...) {
    va_list args;
    va_start(args, format);
    PyObject* res = PyObject_CallFunction(obj, format, args);
    va_end(args);
    checkError();
    return res;
}

PyObj PyObj::callWithArgs(...) {
    va_list args;
    va_start(args, NULL);
    PyObject* res = PyObject_CallFunctionObjArgs(obj, args);
    va_end(args);
    checkError();
    return res;
}

PyObj PyObj::method(const char* method, const char* format, ...) {
    va_list args;
    va_start(args, format);
    PyObject* res = PyObject_CallMethod(obj, method, format, args);
    va_end(args);
    checkError();
    return res;      
}

PyObj PyObj::methodWithArgs(const char* method, ...) {
    va_list args;
    va_start(args, NULL);
    PyObj m = PyUnicode_FromString(method);
    PyObject* res = PyObject_CallMethodObjArgs(obj, m, args);
    va_end(args);
    checkError();
    return res;      
}

bool PyObj::isSubclassOf(PyObj base) {
    bool res = PyObject_IsSubclass(obj, base.obj);
    checkError();
    return res;
}

bool PyObj::isInstanceOf(PyObj what) {
    bool res = PyObject_IsInstance(obj, what.obj);
    checkError();
    return res;    
}

size_t PyObj::lenght() {
    size_t res = PyList_Size(obj);
    checkError();
    return res;
}

void init() {
    Py_Initialize();
}

void checkError() {
    if (PyErr_Occurred()) {
        PyErr_Print();
        throw PyError { };
    }
}

void finalize() {
    PyObj::finalized = true;    
    
    for (auto it = PyObj::refs.begin(); it != PyObj::refs.end(); ++it) {
        for (size_t i = 0; it->first && i < it->second; ++i) {
            Py_DECREF(it->first);
            checkError();
        }
    }
    
    Py_Finalize();
}

PyObj import(const char* module) {
    PyObj name = PyUnicode_DecodeFSDefault(module);
    PyObj m = PyImport_Import(name);
    checkError();
    return m;
}


template <> 
int convert<int>(PyObj obj) {
    int res = PyLong_AsLong(obj);
    checkError();
    return res;
}

template <> 
long convert<long>(PyObj obj) {
    long res = PyLong_AsLong(obj);
    checkError();
    return res;
}

template <> 
double convert<double>(PyObj obj) {
    double res = PyFloat_AsDouble(obj);
    checkError();
    return res;
}

template <> 
float convert<float>(PyObj obj) {
    float res = PyFloat_AsDouble(obj);
    checkError();
    return res;
}

template <> 
char* convert<char*>(PyObj obj) {
    char* res = PyUnicode_AsUTF8(obj);
    checkError();
    return res;
}

template <> 
bool convert<bool>(PyObj obj) {
    if (obj == Py_True) return true;
    if (obj == Py_False) return false;
    throw PyError();
}

template <> 
PyObj convert<PyObj>(const char* format, ...) {
    PyObj res;

    va_list args;
    va_start(args, format);

    if (!(res = Py_BuildValue(format, args))) {
        va_end(args);
        checkError();
        throw PyError { };
    }

    va_end(args);

    return res;
}

}}