#include "DeferredRenderer.hpp"
#include "Timer.hpp"
#include <glm/glm.hpp>
#include <random>
#include <iostream>
#include <iomanip>

using namespace std;

constexpr GLfloat lerp(GLfloat a, GLfloat b, GLfloat f) {
    return a + f * (b - a);
}  

DeferredRenderer::DeferredRenderer(int width, int height) {
    this->width  = width;
    this->height = height;

    glViewport(0, 0, width, height);

    glEnable(GL_CULL_FACE); 
    glFrontFace(GL_CCW);   

    gBuffer                 = new GBuffer(width, height);
    ssaoBuffer              = new FrameBuffer(width, height);
    directionalShadowBuffer = new DepthBuffer(SHADOW_WIDTH, SHADOW_HEIGHT);
    pointShadowBuffer       = new DepthCubeBuffer(SHADOW_WIDTH, SHADOW_HEIGHT);
    postprocBuffer          = new FrameBuffer(width, height);
    
    gBufferShader     = new Shader("shaders/gBufferView.vert", "shaders/gBuffer.frag");
    ssaoShader        = new Shader("shaders/ssao.vert", "shaders/ssao.frag");
    shadowShader      = new Shader("shaders/shadowmap.vert", "shaders/shadowmap.frag");
    cubeShadowShader  = new Shader("shaders/cubeShadowmap.vert", "shaders/cubeShadowmap.geom", "shaders/cubeShadowmap.frag");
    ambientShader     = new Shader("shaders/lightDeffered.vert", "shaders/defferedAmbient.frag");
    directionalShader = new Shader("shaders/lightDeffered.vert", "shaders/defferedDirrectional.frag");
    pointShader       = new Shader("shaders/lightDeffered.vert", "shaders/defferedPoint.frag");
    gammaShader       = new Shader("shaders/gamma.vert", "shaders/gamma.frag");
    debugShader       = new Shader("shaders/debug.vert", "shaders/debug.frag");

    screenPlane = new Plane(2.0f, 2.0f);
    // variable initialization


    // creating noise texture and samples for SSAO 
    default_random_engine generator;
    uniform_real_distribution<GLfloat> randomFloats(0.0, 1.0);

    std::vector<glm::vec3> ssaoNoise;
    for (GLuint i = 0; i < SSAO_NOISE_SIZE * SSAO_NOISE_SIZE; i++) {
        glm::vec3 noise(
            randomFloats(generator) * 2.0f - 1.0f, 
            randomFloats(generator) * 2.0f - 1.0f, 
            0.0f); 
        ssaoNoise.push_back(glm::normalize(noise));
    }
    
    ssaoNoiseTexture = new Texture(SSAO_NOISE_SIZE, SSAO_NOISE_SIZE, GL_RGB16F, GL_RGB, GL_FLOAT, reinterpret_cast<void*>(&ssaoNoise[0])); 
    
    for (GLuint i = 0; i < SSAO_SAMPLES; ++i) {
        glm::vec3 sample(
            randomFloats(generator) * 2.0f - 1.0f, 
            randomFloats(generator) * 2.0f - 1.0f, 
            randomFloats(generator) + 0.0f
        );
    
        sample = glm::normalize(sample);
        //sample *= randomFloats(generator);
        GLfloat scale = GLfloat(i) / 64.0; 
        scale = lerp(0.1f, 1.0f, scale * scale);
        sample *= scale;
        ssaoKernel.push_back(sample);
    }    
}

void DeferredRenderer::setCamera(Camera* c) {
    camera = c;
}

void DeferredRenderer::addObject(Object* o) {
    objects.push_back(o);
}

void DeferredRenderer::addAmbientLight(AmbientLight* l) {
    ambientLights.push_back(l);
}

void DeferredRenderer::addDirectionalLight(DirectionalLight* l) {
    directionalLights.push_back(l);
}

void DeferredRenderer::addPointLight(PointLight* l) {
    pointLights.push_back(l);
}

void DeferredRenderer::removeObject(Object* o) {
    auto it = find(objects.begin(), objects.end(), o);
    if (it != objects.end()) objects.erase(it);
}

void DeferredRenderer::removeAmbientLight(AmbientLight* l) {
    auto it = find(ambientLights.begin(), ambientLights.end(), l);
    if (it != ambientLights.end()) ambientLights.erase(it);
}

void DeferredRenderer::removeDirectionalLight(DirectionalLight* l) {
    auto it = find(directionalLights.begin(), directionalLights.end(), l);
    if (it != directionalLights.end()) directionalLights.erase(it);
}


void DeferredRenderer::removePointLight(PointLight* l) {
    auto it = find(pointLights.begin(), pointLights.end(), l);
    if (it != pointLights.end()) pointLights.erase(it);
}

void DeferredRenderer::draw() {
    Timer allT;
    
    Timer gbT;
    fillGBuffer();
    gbT.stop();

    Timer ssaoT;
    calculateSSAO();
    ssaoT.stop();
    
    Timer lT;
    postprocBuffer->bind();
    glClear(GL_COLOR_BUFFER_BIT);
    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE);

    drawAmbientLights();
    drawDirectionalLights();
    drawPointLights();
    
    glDisable(GL_BLEND);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    doGammaCorrection();

    lT.stop();
    allT.stop();

    cout << "FPS:"    << setw(3) << 1000 / allT.getTime()
         << "  TOTAL:" << setw(3) << allT.getTime()   << "ms"
         << "  GB:"    << setw(3) << gbT.getTime()    << "ms"
         << "  SSAO:"  << setw(2) << ssaoT.getTime()  << "ms"
         << "  LIGHT:" << setw(3) << lT.getTime()     << "ms"
         << endl; 
}

void DeferredRenderer::doGammaCorrection() {
    gammaShader->bind();
    glActiveTexture(GL_TEXTURE0);
    postprocBuffer->getColorTexture()->bind();
    gammaShader->setUniform("gamma", gamma);
    screenPlane->draw();
}


void DeferredRenderer::drawSSAOMask() {
    fillGBuffer();
    calculateSSAO();
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glActiveTexture(GL_TEXTURE0);
    debugShader->bind();
    ssaoBuffer->getColorTexture()->bind();
    screenPlane->draw();
}

void DeferredRenderer::drawNormals() {
    fillGBuffer();
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glActiveTexture(GL_TEXTURE0);
    debugShader->bind();
    gBuffer->getNormalTexture()->bind();
    screenPlane->draw();
}

void DeferredRenderer::drawColorMask() {
    fillGBuffer();
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glActiveTexture(GL_TEXTURE0);
    debugShader->bind();
    gBuffer->getAlbedoTexture()->bind();
    screenPlane->draw();
}

void DeferredRenderer::drawSpecMask() {
    fillGBuffer();
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glActiveTexture(GL_TEXTURE0);
    debugShader->bind();
    gBuffer->getSpecTexture()->bind();
    screenPlane->draw();
}

void DeferredRenderer::drawPositions() {
    fillGBuffer();
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glActiveTexture(GL_TEXTURE0);
    debugShader->bind();
    gBuffer->getPositionTexture()->bind();
    screenPlane->draw();
}

void DeferredRenderer::drawAmbientOnly() {
    fillGBuffer();
    calculateSSAO();

    postprocBuffer->bind();
    glClear(GL_COLOR_BUFFER_BIT);
    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE);
    
    drawAmbientLights();

    glDisable(GL_BLEND);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    doGammaCorrection();
}

void DeferredRenderer::drawDirectionalOnly() {
    fillGBuffer();

    postprocBuffer->bind();
    glClear(GL_COLOR_BUFFER_BIT);
    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE);
    
    drawDirectionalLights();

    glDisable(GL_BLEND);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    doGammaCorrection();
}

void DeferredRenderer::drawPointOnly() {
    fillGBuffer();
    
    postprocBuffer->bind();
    glClear(GL_COLOR_BUFFER_BIT);
    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE);
    
    drawPointLights();

    glDisable(GL_BLEND);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    doGammaCorrection();
}

void DeferredRenderer::fillGBuffer() {
    gBuffer->bind();
    gBufferShader->bind();
    
    glEnable(GL_DEPTH_TEST);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    gBufferShader->setUniform("view"      , camera->getViewMatrix());
    gBufferShader->setUniform("projection", camera->getProjectionMatrix());
    gBufferShader->setUniform("cameraPos" , camera->getPosition());
    
    gBufferShader->setUniform("gamma", gamma);

    gBufferShader->setUniform("material.diffuseTexture" , 0);
    gBufferShader->setUniform("material.specularTexture", 1);
    gBufferShader->setUniform("material.normalTexture"  , 2);
    
    for (int i = 0; i < objects.size(); ++i) {
        Transform* tr = objects[i]->transform;
        gBufferShader->setUniform("model", tr->getMatrix());

        for (int j = 0; j < objects[i]->meshes.size(); ++j) {
            Mesh* m = objects[i]->meshes[j];
            Material* mt = objects[i]->materials[j];
            
            gBufferShader->setUniform("material.ambient"  , mt->ambient);
            gBufferShader->setUniform("material.diffuse"  , mt->diffuse);
            gBufferShader->setUniform("material.specular" , mt->specular);
            gBufferShader->setUniform("material.shininess", mt->shininess);
        
            glActiveTexture(GL_TEXTURE0);
            mt->diffuseTexture->bind();
            glActiveTexture(GL_TEXTURE1);
            mt->specularTexture->bind();
            glActiveTexture(GL_TEXTURE2);
            mt->normalTexture->bind();

            m->draw();
        }
    }

    glDisable(GL_DEPTH_TEST);
}

void DeferredRenderer::calculateSSAO() {
    ssaoBuffer->bind();
    ssaoShader->bind();

    for (int i = 0; i < SSAO_SAMPLES; ++i) {
        ssaoShader->setUniform("samples[" + std::to_string(i) + "]", ssaoKernel[i]);
    }

    ssaoShader->setUniform("projection", camera->getProjectionMatrix());

    ssaoShader->setUniform("gPositionDepth", 0);
    ssaoShader->setUniform("gNormal"       , 1);
    ssaoShader->setUniform("texNoise"      , 2);

    glActiveTexture(GL_TEXTURE0);
    gBuffer->getPositionTexture()->bind();

    glActiveTexture(GL_TEXTURE1);
    gBuffer->getNormalTexture()->bind();

    glActiveTexture(GL_TEXTURE2);
    ssaoNoiseTexture->bind();

    screenPlane->draw();
}

glm::mat4 DeferredRenderer::calculateDirrectionalShadow(DirectionalLight* l) {
    directionalShadowBuffer->bind();
    shadowShader->bind();

    glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
    glEnable(GL_DEPTH_TEST);
    glClear(GL_DEPTH_BUFFER_BIT);

    // TODO
    GLfloat near_plane = 0.1f, far_plane = 20.0f;
    glm::mat4 lightProjection = glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, near_plane, far_plane);  
    glm::mat4 lightView = glm::lookAt(l->direction * (-10.0),
                                      glm::vec3(0.0f, 0.0f, 0.0f),
                                      glm::vec3(0.0f, 1.0f, 0.0f));

    glm::mat4 lightSpaceMatrix = lightProjection * lightView;
    
    shadowShader->setUniform("lightSpaceMatrix", lightSpaceMatrix);

    for (int i = 0; i < objects.size(); ++i) {
        shadowShader->setUniform("model" , objects[i]->transform->getMatrix());

        for (int j = 0; j < objects[i]->meshes.size(); ++j) {
            objects[i]->meshes[j]->draw();
        }
    }

    directionalShader->bind();
    postprocBuffer->bind();
    glViewport(0, 0, width, height);
    glDisable(GL_DEPTH_TEST);
    
    return lightSpaceMatrix;
}

void DeferredRenderer::calculatePointShadow(PointLight* l) {
    glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
    pointShadowBuffer->bind();
    glEnable(GL_DEPTH_TEST);
    glClear(GL_DEPTH_BUFFER_BIT);

    GLfloat aspect = (GLfloat)SHADOW_WIDTH / (GLfloat)SHADOW_HEIGHT;

    GLfloat near = 0.1f;
    GLfloat far = 30.0f;

    glm::mat4 shadowProj = glm::perspective(glm::radians(90.0f), aspect, near, far);
    std::vector<glm::mat4> shadowTransforms;

    shadowTransforms.push_back(shadowProj * glm::lookAt(l->position, l->position + glm::vec3( 1.0,  0.0,  0.0), glm::vec3(0.0, -1.0,  0.0)));
    shadowTransforms.push_back(shadowProj * glm::lookAt(l->position, l->position + glm::vec3(-1.0,  0.0,  0.0), glm::vec3(0.0, -1.0,  0.0)));
    shadowTransforms.push_back(shadowProj * glm::lookAt(l->position, l->position + glm::vec3( 0.0,  1.0,  0.0), glm::vec3(0.0,  0.0,  1.0)));
    shadowTransforms.push_back(shadowProj * glm::lookAt(l->position, l->position + glm::vec3( 0.0, -1.0,  0.0), glm::vec3(0.0,  0.0, -1.0)));
    shadowTransforms.push_back(shadowProj * glm::lookAt(l->position, l->position + glm::vec3( 0.0,  0.0,  1.0), glm::vec3(0.0, -1.0,  0.0)));
    shadowTransforms.push_back(shadowProj * glm::lookAt(l->position, l->position + glm::vec3( 0.0,  0.0, -1.0), glm::vec3(0.0, -1.0,  0.0)));   
    
    cubeShadowShader->bind();

    for (GLuint i = 0; i < 6; ++i) {
        cubeShadowShader->setUniform("shadowMatrices[" + std::to_string(i) + "]", shadowTransforms[i]);
    }
    
    cubeShadowShader->setUniform("farPlane", far);
    cubeShadowShader->setUniform("lightPos", l->position);

    for (int i = 0; i < objects.size(); ++i) {
        cubeShadowShader->setUniform("model", objects[i]->transform->getMatrix());

        for (int j = 0; j < objects[i]->meshes.size(); ++j) {
            objects[i]->meshes[j]->draw();
        }
    }

    pointShader->bind();
    postprocBuffer->bind();
    glViewport(0, 0, width, height);
    glDisable(GL_DEPTH_TEST);
}

void DeferredRenderer::drawAmbientLights() {
    postprocBuffer->bind();
    ambientShader->bind();
    
    ambientShader->setUniform("gAlbedo" , 0);
    ambientShader->setUniform("ssaoMask", 1);
    
    for (int i = 0; i < ambientLights.size(); ++i) {
        AmbientLight* l = ambientLights[i];
        
        glActiveTexture(GL_TEXTURE0);
        gBuffer->getAlbedoTexture()->bind();

        glActiveTexture(GL_TEXTURE1);
        ssaoBuffer->getColorTexture()->bind();

        ambientShader->setUniform("diffuse"  , l->diffuse);
        ambientShader->setUniform("intensity", l->intensity);

        screenPlane->draw();
    }
}

void DeferredRenderer::drawDirectionalLights() {
    postprocBuffer->bind();
    directionalShader->bind();
    
    directionalShader->setUniform("gPosition", 0);
    directionalShader->setUniform("gNormal"  , 1);
    directionalShader->setUniform("gAlbedo"  , 2);
    directionalShader->setUniform("gSpec"    , 3);
    directionalShader->setUniform("shadowMap", 4);

    directionalShader->setUniform("cameraPos", glm::vec3{0,0,0});
    directionalShader->setUniform("shininess", 50.0f);

    for (int i = 0; i < directionalLights.size(); ++i) {
        DirectionalLight* l = directionalLights[i];

        glm::mat4 lightSpaceMatrix = calculateDirrectionalShadow(l);
        
        glActiveTexture(GL_TEXTURE0);
        gBuffer->getPositionTexture()->bind();

        glActiveTexture(GL_TEXTURE1);
        gBuffer->getNormalTexture()->bind();

        glActiveTexture(GL_TEXTURE2);
        gBuffer->getAlbedoTexture()->bind();
    
        glActiveTexture(GL_TEXTURE3);
        gBuffer->getSpecTexture()->bind();
        
        glActiveTexture(GL_TEXTURE4);
        directionalShadowBuffer->getDepthTexture()->bind();
        
        directionalShader->setUniform("shadowMat", lightSpaceMatrix * glm::inverse(camera->getViewMatrix()));
        directionalShader->setUniform("direction", glm::vec3{ camera->getViewMatrix() * glm::vec4{ l->direction, 0 } });
        directionalShader->setUniform("diffuse"  , l->diffuse);
        directionalShader->setUniform("intensity", l->intensity);

        screenPlane->draw();
    }
}

void DeferredRenderer::drawPointLights() {
    postprocBuffer->bind();
    pointShader->bind();

    pointShader->setUniform("gPosition", 0);
    pointShader->setUniform("gNormal"  , 1);
    pointShader->setUniform("gAlbedo"  , 2);
    pointShader->setUniform("gSpec"    , 3);
    pointShader->setUniform("depthMap" , 4);

    pointShader->setUniform("cameraPos", glm::vec3{0,0,0});
    pointShader->setUniform("shininess", 50.0f);
    pointShader->setUniform("inversedView", glm::inverse(camera->getViewMatrix()));

    glm::vec3 sampleOffsetDirections[24] = {
        glm::vec3{ 1,  1,  1 }, glm::vec3{ 1, -1,  1 }, glm::vec3{-1, -1,  1 }, glm::vec3{-1,  1,  1 },
        glm::vec3{ 1,  1, -1 }, glm::vec3{ 1, -1, -1 }, glm::vec3{-1, -1, -1 }, glm::vec3{-1,  1, -1 },
        glm::vec3{ 1,  1,  0 }, glm::vec3{ 1, -1,  0 }, glm::vec3{-1, -1,  0 }, glm::vec3{-1,  1,  0 },
        glm::vec3{ 1,  1,  0 }, glm::vec3{ 1, -1,  0 }, glm::vec3{-1, -1,  0 }, glm::vec3{-1,  1,  0 },
        glm::vec3{ 1,  0,  1 }, glm::vec3{-1,  0,  1 }, glm::vec3{ 1,  0, -1 }, glm::vec3{-1,  0, -1 },
        glm::vec3{ 0,  1,  1 }, glm::vec3{ 0, -1,  1 }, glm::vec3{ 0, -1, -1 }, glm::vec3{ 0,  1, -1 }
    };

    for (GLuint i = 0; i < 24; ++i) {
        pointShader->setUniform("shadowSamples[" + std::to_string(i) + "]", sampleOffsetDirections[i]);
    }
    
    for (int i = 0; i < pointLights.size(); ++i) {
        PointLight* l = pointLights[i];
        
        calculatePointShadow(l);
        
        glActiveTexture(GL_TEXTURE0);
        gBuffer->getPositionTexture()->bind();

        glActiveTexture(GL_TEXTURE1);
        gBuffer->getNormalTexture()->bind();

        glActiveTexture(GL_TEXTURE2);
        gBuffer->getAlbedoTexture()->bind();
    
        glActiveTexture(GL_TEXTURE3);
        gBuffer->getSpecTexture()->bind();

        glActiveTexture(GL_TEXTURE4);
        pointShadowBuffer->getDepthCubeMap()->bind();

        
        pointShader->setUniform("diffuse"  , l->diffuse);
        pointShader->setUniform("position" , l->position);
        pointShader->setUniform("intensity", l->intensity);
        pointShader->setUniform("constant" , l->constant);
        pointShader->setUniform("linear"   , l->linear);
        pointShader->setUniform("quadratic", l->quadratic);
        pointShader->setUniform("position" , glm::vec3{ camera->getViewMatrix() * glm::vec4{ l->position, 1.0f } });
    
        screenPlane->draw();
    }
}
