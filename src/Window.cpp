#include "Window.hpp"
#include "Input.hpp"
#include <iostream>

Window::Window(std::string title, int width, int height) : width{width}, height{height} {
    
    glfwSetErrorCallback(errorCallback);

    if (!glfwInit()) {
        std::cerr << "GLFW initialization error!" << std::endl;
        return;
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);    
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    //glfwWindowHint(GLFW_SAMPLES, 4);

    window = glfwCreateWindow(width, height, title.c_str(), NULL, NULL);

    if (!window) {
        std::cerr << "Window creation error!" << std::endl;
        return;
    }

    input = new Input();

    glfwSetWindowUserPointer(window, this);

    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);
    glfwSetKeyCallback(window, keyCallback);
    glfwSetMouseButtonCallback(window, mouseButtonCallback);
    glfwSetCursorPosCallback(window, cursorPositionCallback);
    
    glfwMakeContextCurrent(window);

    glfwSwapInterval(1);

    glewExperimental = true;
    GLenum err = glewInit();
    if(err != GLEW_OK) {
        std::cerr << "GLEW initializartion error!" << std::endl;
        std::cerr << glewGetErrorString(err) << std::endl;
        return;
    }
}

Window::~Window() {
    input->~Input();
    glfwDestroyWindow(window);
    glfwTerminate();
}

Input* Window::getInput() {
    return input;
}

bool Window::shouldClose() {
    return glfwWindowShouldClose(window);
}

void Window::updateInput() {
    input->update();
    glfwPollEvents();
}

void Window::swapBuffers() {
    glFlush();
    glfwSwapBuffers(window);
}

void Window::setCursorMode(int mode) {
    glfwSetInputMode(window, GLFW_CURSOR, mode);
}

int Window::getWidth() {
    return width;
}

int Window::getHeight() {
    return height;
}

void Window::errorCallback(int error, const char* description) { 
    std::cerr << "Error " << error << ": " << description << std::endl;
}

void Window::framebufferSizeCallback(GLFWwindow* window, int w, int h) {
    Window* win = reinterpret_cast<Window*>(glfwGetWindowUserPointer(window));
    win->width = w;
    win->height = h;
    glViewport(0, 0, w, h);
}

void Window::keyCallback(GLFWwindow* window, int key, int /*scancode*/, int action, int /*mods*/) {
    Window* win = reinterpret_cast<Window*>(glfwGetWindowUserPointer(window));
    win->input->keyboardEvent(key, action);
}

void Window::mouseButtonCallback(GLFWwindow* window, int button, int action, int /*mods*/) {
    Window* win = reinterpret_cast<Window*>(glfwGetWindowUserPointer(window));
    win->input->mouseEvent(button, action);
}

void Window::cursorPositionCallback(GLFWwindow* window, double xpos, double ypos) {
    Window* win = reinterpret_cast<Window*>(glfwGetWindowUserPointer(window));
    win->input->mousePosEvent(static_cast<int>(xpos), static_cast<int>(ypos));
}
