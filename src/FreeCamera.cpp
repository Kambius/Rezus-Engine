#include "FreeCamera.hpp"
#include "Engine.hpp"

FreeCamera::FreeCamera() : 
    FreeCamera{glm::vec3{0,0,0}, glm::vec3{0,1,0}, glm::vec3{0,0,-1}, 5.0, 0.2} { }

FreeCamera::FreeCamera(glm::vec3 pos, glm::vec3 up, glm::vec3 front, float speed, float sensitivity) :
    Camera{pos, up, front}, speed{speed}, firstUpdate{true}, sensitivity{sensitivity} { }

void FreeCamera::update() {
    if (firstUpdate) {
        firstUpdate = false;
        lastUpdateTime = glfwGetTime();
    }

    float now = glfwGetTime();
    float delta = now - lastUpdateTime;
    lastUpdateTime = now;
    
    if (Engine::getInput().isKeyDown(KEY_W))
        moveForward(speed * delta);
    if (Engine::getInput().isKeyDown(KEY_S))
        moveBack(speed * delta);
    if (Engine::getInput().isKeyDown(KEY_A))
        moveLeft(speed * delta);
    if (Engine::getInput().isKeyDown(KEY_D))
        moveRight(speed * delta); 
    if (Engine::getInput().isKeyDown(KEY_SPACE))
        moveUp(speed * delta);
    if (Engine::getInput().isKeyDown(KEY_LEFT_SHIFT))
        moveDown(speed * delta); 

    float xoffset =-Engine::getInput().getCursorDeltaX() * sensitivity;
    float yoffset =-Engine::getInput().getCursorDeltaY() * sensitivity; 

    setYaw(getYaw() + xoffset);
    setPitch(getPitch() + yoffset);
}
    
float FreeCamera::getSpeed() {
    return speed;
}

float FreeCamera::getSensitivity() {
    return sensitivity;
}

void FreeCamera::setSensitivity(float s) {
    sensitivity = s;
}
