#include "Plane.hpp"

using namespace std;

Plane::Plane(float width, float height)
    : Mesh{
        vector<Vertex> {
            Vertex{ {-width/2, height/2, 0 }, { 0, 0, 1 }, { 0, 1 }, { 1, 0, 0 }, { 0, 1, 0 } },
            Vertex{ { width/2, height/2, 0 }, { 0, 0, 1 }, { 1, 1 }, { 1, 0, 0 }, { 0, 1, 0 }  },
            Vertex{ { width/2,-height/2, 0 }, { 0, 0, 1 }, { 1, 0 }, { 1, 0, 0 }, { 0, 1, 0 }  },
            Vertex{ {-width/2,-height/2, 0 }, { 0, 0, 1 }, { 0, 0 }, { 1, 0, 0 }, { 0, 1, 0 }  },
        }, vector<GLuint> { 0, 3, 1, 1, 3, 2 } 
    } { }