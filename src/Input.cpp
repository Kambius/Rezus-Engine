#include "Input.hpp"
#include <iostream>

Input::Input() : cursorX{}, cursorY{},
                 keysPressed{}, 
                 keysReleased{},
                 keysDown{},
                 buttonsPressed{},
                 buttonsReleased{},
                 buttonsDown{} { }
 
bool Input::isKeyPressed(int key) {
    return keysPressed[key];
}

bool Input::isKeyReleased(int key) {
    return keysReleased[key];
}

bool Input::isKeyDown(int key) {
    return keysDown[key];
}

bool Input::isMousePressed(int button) {
    return buttonsPressed[button];
}

bool Input::isMouseReleased(int button) {
    return buttonsReleased[button];
}

bool Input::isMouseDown(int button) {
    return buttonsDown[button];
}

int Input::getCursorX() {
    return cursorX;
}

int Input::getCursorY() {
    return cursorY;
}

int Input::getCursorDeltaX() {
    return cursorX - lastCursorX;
}

int Input::getCursorDeltaY() {
    return cursorY - lastCursorY;
}

void Input::update() {
    for (int i = 0; i < KEYS_COUNT; ++i) {
        keysPressed[i] = false;
        keysReleased[i] = false;
    }

    for (int i = 0; i < BUTTONS_COUNT; ++i) {
        buttonsReleased[i] = false;
        buttonsPressed[i] = false;
    }

    lastCursorX = cursorX;
    lastCursorY = cursorY;
}

void Input::keyboardEvent(int key, int type) {
    if (type == ACTION_RELEASE) {
        keysDown[key] = false;
        keysReleased[key] = true;
    } else {
        keysDown[key] = true;
        keysPressed[key] = true;
    }
}

void Input::mouseEvent(int button, int type) {
    if (type == ACTION_RELEASE) {
        buttonsDown[button] = false;
        buttonsReleased[button] = true;
    } else {
        buttonsDown[button] = true;
        buttonsPressed[button] = true;
    }
}

void Input::mousePosEvent(int x, int y) {
    cursorX = x;
    cursorY = y;
}