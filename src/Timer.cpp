#include "Timer.hpp"
#include <GLFW/glfw3.h>


Timer::Timer() {
    reset();
}

void Timer::reset() {
    startTime = glfwGetTime();
}

int Timer::stop() {
    return (time = glfwGetTime() - startTime) * 1000;
}

int Timer::getTime() {
    return time * 1000;
}
