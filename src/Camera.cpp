#include "Camera.hpp"
#include "Engine.hpp"
#include <cmath>

using namespace std;

Camera::Camera() 
    : Camera{glm::vec3{0,0,0}, glm::vec3{0,1,0}, glm::vec3{0,0,-1}} { }

Camera::Camera(glm::vec3 pos, glm::vec3 up, glm::vec3 front) 
    : position{pos}
    , up{up}
    , front{front} { }

glm::mat4 Camera::getViewMatrix() {
    return glm::lookAt(position, position + front, up);
}

glm::mat4 Camera::getProjectionMatrix() {
    return glm::perspective(1.0f, 
        Engine::getWindow().getWidth() / static_cast<GLfloat>(Engine::getWindow().getHeight()), 
        0.1f, 100.0f);
}

glm::vec3 Camera::getPosition() {
    return position;
}

glm::vec3 Camera::getUp() {
    return up;
}

glm::vec3 Camera::getFront() {
    return front;
}

float Camera::getPitch() {
    return glm::degrees(std::asin(glm::normalize(front).y));
}

float Camera::getYaw() {  
    return glm::degrees(atan2(glm::normalize(front).x, glm::normalize(front).z));    
}

void Camera::setPosition(glm::vec3 pos) {
    position = pos;
}

void Camera::setUp(glm::vec3 up) {
    this->up = up;
}

void Camera::setFront(glm::vec3 front) {
    this->front = front;
}

void Camera::setPitch(float p) {
    if (p > 89.0f) p = 89.0f;
    if (p <-89.0f) p =-89.0f;

    float y = getYaw();
    glm::vec3 f;
    
    f.x = cos(glm::radians(y)) * cos(glm::radians(p));
    f.y = sin(glm::radians(p));
    f.z = sin(glm::radians(y)) * cos(glm::radians(p));

    setFront(f);
}

void Camera::setYaw(float y) {
    float p = getPitch();
    glm::vec3 f;
    
    f.x = cos(glm::radians(y)) * cos(glm::radians(p));
    f.y = sin(glm::radians(p));
    f.z = sin(glm::radians(y)) * cos(glm::radians(p));

    setFront(f);
}

void Camera::moveForward(float amount) {
    position += normalize(front) * amount;
}

void Camera::moveBack(float amount) {
    position -= normalize(front) * amount;
}

void Camera::moveUp(float amount) {
    position += glm::normalize(glm::cross(glm::cross(front, up), front)) * amount;
}

void Camera::moveDown(float amount) {
    position -= glm::normalize(glm::cross(glm::cross(front, up), front)) * amount;
}

void Camera::moveLeft(float amount) {
    position -= glm::normalize(glm::cross(front, up)) * amount;
}

void Camera::moveRight(float amount) {
    position += glm::normalize(glm::cross(front, up)) * amount;
}

void Camera::lookAt(glm::vec3 point) {
    front = position - point;
}

