#include "Cube.hpp"

using namespace std;

Cube::Cube(float s)
    : Mesh {
        vector<Vertex> {
            // Back face
            Vertex{ {-s/2,-s/2,-s/2 }, { 0.0f, 0.0f,-1.0f }, { 0.0f, 0.0f } }, // Bottom-left
            Vertex{ { s/2, s/2,-s/2 }, { 0.0f, 0.0f,-1.0f }, { 1.0f, 1.0f } }, // top-right
            Vertex{ { s/2,-s/2,-s/2 }, { 0.0f, 0.0f,-1.0f }, { 1.0f, 0.0f } }, // bottom-right
            Vertex{ { s/2, s/2,-s/2 }, { 0.0f, 0.0f,-1.0f }, { 1.0f, 1.0f } }, // top-right
            Vertex{ {-s/2,-s/2,-s/2 }, { 0.0f, 0.0f,-1.0f }, { 0.0f, 0.0f } }, // bottom-left
            Vertex{ {-s/2, s/2,-s/2 }, { 0.0f, 0.0f,-1.0f }, { 0.0f, 1.0f } }, // top-left
            // Front face
            Vertex{ {-s/2,-s/2, s/2 }, { 0.0f, 0.0f, 1.0f }, { 0.0f, 0.0f } }, // bottom-left
            Vertex{ { s/2,-s/2, s/2 }, { 0.0f, 0.0f, 1.0f }, { 1.0f, 0.0f } }, // bottom-right
            Vertex{ { s/2, s/2, s/2 }, { 0.0f, 0.0f, 1.0f }, { 1.0f, 1.0f } }, // top-right
            Vertex{ { s/2, s/2, s/2 }, { 0.0f, 0.0f, 1.0f }, { 1.0f, 1.0f } }, // top-right
            Vertex{ {-s/2, s/2, s/2 }, { 0.0f, 0.0f, 1.0f }, { 0.0f, 1.0f } }, // top-left
            Vertex{ {-s/2,-s/2, s/2 }, { 0.0f, 0.0f, 1.0f }, { 0.0f, 0.0f } }, // bottom-left
            // Left face
            Vertex{ {-s/2, s/2, s/2 }, {-1.0f, 0.0f, 0.0f }, { 1.0f, 0.0f } }, // top-right
            Vertex{ {-s/2, s/2,-s/2 }, {-1.0f, 0.0f, 0.0f }, { 1.0f, 1.0f } }, // top-left
            Vertex{ {-s/2,-s/2,-s/2 }, {-1.0f, 0.0f, 0.0f }, { 0.0f, 1.0f } }, // bottom-left
            Vertex{ {-s/2,-s/2,-s/2 }, {-1.0f, 0.0f, 0.0f }, { 0.0f, 1.0f } }, // bottom-left
            Vertex{ {-s/2,-s/2, s/2 }, {-1.0f, 0.0f, 0.0f }, { 0.0f, 0.0f } }, // bottom-right
            Vertex{ {-s/2, s/2, s/2 }, {-1.0f, 0.0f, 0.0f }, { 1.0f, 0.0f } }, // top-right
            // Right face
            Vertex{ { s/2, s/2, s/2 }, { 1.0f, 0.0f, 0.0f }, { 1.0f, 0.0f } }, // top-left
            Vertex{ { s/2,-s/2,-s/2 }, { 1.0f, 0.0f, 0.0f }, { 0.0f, 1.0f } }, // bottom-right
            Vertex{ { s/2, s/2,-s/2 }, { 1.0f, 0.0f, 0.0f }, { 1.0f, 1.0f } }, // top-right
            Vertex{ { s/2,-s/2,-s/2 }, { 1.0f, 0.0f, 0.0f }, { 0.0f, 1.0f } }, // bottom-right
            Vertex{ { s/2, s/2, s/2 }, { 1.0f, 0.0f, 0.0f }, { 1.0f, 0.0f } }, // top-left
            Vertex{ { s/2,-s/2, s/2 }, { 1.0f, 0.0f, 0.0f }, { 0.0f, 0.0f } }, // bottom-left
            // Bottom face
            Vertex{ {-s/2,-s/2,-s/2 }, { 0.0f,-1.0f, 0.0f }, { 0.0f, 1.0f } }, // top-right
            Vertex{ { s/2,-s/2,-s/2 }, { 0.0f,-1.0f, 0.0f }, { 1.0f, 1.0f } }, // top-left
            Vertex{ { s/2,-s/2, s/2 }, { 0.0f,-1.0f, 0.0f }, { 1.0f, 0.0f } }, // bottom-left
            Vertex{ { s/2,-s/2, s/2 }, { 0.0f,-1.0f, 0.0f }, { 1.0f, 0.0f } }, // bottom-left
            Vertex{ {-s/2,-s/2, s/2 }, { 0.0f,-1.0f, 0.0f }, { 0.0f, 0.0f } }, // bottom-right
            Vertex{ {-s/2,-s/2,-s/2 }, { 0.0f,-1.0f, 0.0f }, { 0.0f, 1.0f } }, // top-right
            // Top face
            Vertex{ {-s/2, s/2,-s/2 }, { 0.0f, 1.0f, 0.0f }, { 0.0f, 1.0f } }, // top-left
            Vertex{ { s/2, s/2, s/2 }, { 0.0f, 1.0f, 0.0f }, { 1.0f, 0.0f } }, // bottom-right
            Vertex{ { s/2, s/2,-s/2 }, { 0.0f, 1.0f, 0.0f }, { 1.0f, 1.0f } }, // top-right
            Vertex{ { s/2, s/2, s/2 }, { 0.0f, 1.0f, 0.0f }, { 1.0f, 0.0f } }, // bottom-right
            Vertex{ {-s/2, s/2,-s/2 }, { 0.0f, 1.0f, 0.0f }, { 0.0f, 1.0f } }, // top-left
            Vertex{ {-s/2, s/2, s/2 }, { 0.0f, 1.0f, 0.0f }, { 0.0f, 0.0f } }  // bottom-left
        }, vector<GLuint> {
            0 , 1 , 2 , 3 , 4 , 5 , 6 , 7 , 8 , 9 , 10, 11, 12, 13, 14, 15, 16, 17,
            18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35 
        }
    } { }
