#include "Engine.hpp"
#include "DeferredRenderer.hpp"
#include "FreeCamera.hpp"
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

FreeCamera* camera;
DeferredRenderer* renderer;

enum DisplayMode {
    FULL,
    NORMALS,
    POSITIONS,
    SSAO,
    COLOR,
    SPEC,
    AMBIENT,
    DIRECTIONAL,
    POINT
};

DisplayMode mode; // Current mode


// Undates logic, habdles user input
void update() {
    // Updates camera position
    camera->update();

    // Handl exit
    if (Engine::getInput().isKeyPressed(KEY_ESCAPE)) {
        exit(0);
    }

    // Hide cursor
    if (Engine::getInput().isMousePressed(BUTTON_RIGHT)) {
        Engine::getWindow().setCursorMode(CURSOR_NORMAL);
    }

    // Show cusor
    if (Engine::getInput().isMousePressed(BUTTON_LEFT)) {
        Engine::getWindow().setCursorMode(CURSOR_DISABLED);
    }

    // Switches display nodes
    if (Engine::getInput().isKeyPressed(KEY_0)) mode = FULL;
    if (Engine::getInput().isKeyPressed(KEY_1)) mode = NORMALS;
    if (Engine::getInput().isKeyPressed(KEY_2)) mode = POSITIONS;
    if (Engine::getInput().isKeyPressed(KEY_3)) mode = SSAO;
    if (Engine::getInput().isKeyPressed(KEY_4)) mode = COLOR;
    if (Engine::getInput().isKeyPressed(KEY_5)) mode = SPEC;    
    if (Engine::getInput().isKeyPressed(KEY_6)) mode = AMBIENT; 
    if (Engine::getInput().isKeyPressed(KEY_7)) mode = DIRECTIONAL; 
    if (Engine::getInput().isKeyPressed(KEY_8)) mode = POINT;   
    if (Engine::getInput().isKeyPressed(KEY_9)) {
        renderer->gamma = renderer->gamma == 1.0 ? 1.7f : 1.0f; // toggle gamma corection
    }   
}

// Renders scene in selected mode
void draw() {
    switch (mode) {
        case FULL:        renderer->draw();                break; 
        case NORMALS:     renderer->drawNormals();         break; 
        case POSITIONS:   renderer->drawPositions();       break; 
        case SSAO:        renderer->drawSSAOMask();        break; 
        case COLOR:       renderer->drawColorMask();       break; 
        case SPEC:        renderer->drawSpecMask();        break;  
        case AMBIENT:     renderer->drawAmbientOnly();     break; 
        case DIRECTIONAL: renderer->drawDirectionalOnly(); break; 
        case POINT:       renderer->drawPointOnly();       break; 
    }
}


// Loads scene described in file
void loadScene(string filename) {

    ifstream file;
    file.open(filename);
   
    if (!file.is_open()) return;

    Object* obj = nullptr;
    while (!file.eof()) {
        string token;
        file >> token;

        // Mpdel
        if (token == "md") {
            string mdFile;
            file >> mdFile;
            obj = new Object(mdFile);
            renderer->addObject(obj);
        } 

        // Translation
        if (token == "tr" && obj != nullptr) {
            glm::vec3 tr;
            file >> tr.x >> tr.y >> tr.z;
            obj->transform->translate(tr);
        }

        // Svale
        if (token == "sc" && obj != nullptr) {
            glm::vec3 sc;
            file >> sc.x >> sc.y >> sc.z;
            obj->transform->scale(sc);
        }       

        // Rotation
        if (token == "rt" && obj != nullptr) {
            glm::vec3 rt;
            float ang;
            file >> ang >> rt.x >> rt.y >> rt.z;
            obj->transform->rotate(ang, rt);
        }       

        // Material
        if (token == "mt" && obj != nullptr) {
            for (int i = 0; i < obj->materials.size(); ++i) {
                Material* m = obj->materials[i];
                file >> m->diffuse.r >> m->diffuse.g >> m->diffuse.b;
                file >> m->specular.r >> m->specular.g >> m->specular.b;
                string texFile;
                file >> texFile;
                if (texFile != "null") m->diffuseTexture = new Texture(texFile);
                file >> texFile;
                if (texFile != "null") m->specularTexture = new Texture(texFile);
                file >> texFile;
                if (texFile != "null") m->normalTexture = new Texture(texFile);
                file >> m->shininess;
            }
        }
        
        // Ambient light
        if (token == "al") {
            AmbientLight* l = new AmbientLight(); 
            file >> l->diffuse.r >> l->diffuse.g >> l->diffuse.b;
            file >> l->intensity;
            renderer->addAmbientLight(l);
        }

        // Directional light
        if (token == "dl") {
            DirectionalLight* l = new DirectionalLight();
            file >> l->diffuse.r >> l->diffuse.g >> l->diffuse.b;
            file >> l->intensity;
            file >> l->direction.x >> l->direction.y >> l->direction.z;
            renderer->addDirectionalLight(l);
        }

        // Point light
        if (token == "pl") {
            PointLight* l = new PointLight();
            file >> l->diffuse.r >> l->diffuse.g >> l->diffuse.b;
            file >> l->intensity;
            file >> l->position.x >> l->position.y >> l->position.z;
            file >> l->constant >> l->linear >> l->quadratic;
            renderer->addPointLight(l);
        }
    }

    file.close();
}

int main() {
    Engine::init();
        
    camera = new FreeCamera();
    renderer = new DeferredRenderer(Engine::getWindow().getWidth(), Engine::getWindow().getHeight());
    loadScene("scene.dat");

    renderer->setCamera(camera);
    
    Engine::setUpdate(update);
    Engine::setDraw(draw);
    Engine::start();

    return 0;
}
