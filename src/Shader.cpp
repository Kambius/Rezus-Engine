#include "Shader.hpp"

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

Shader::Shader(string vertexFilePath, string fragmentFilePath) {
    shaderID = loadShaders(vertexFilePath, "null", fragmentFilePath);
}

Shader::Shader(std::string vertexFilePath, std::string geometryFilePath, std::string fragmentFilePath) {
    shaderID = loadShaders(vertexFilePath, geometryFilePath, fragmentFilePath);
}

void Shader::dispose() {
    glUseProgram(0);
    glDeleteProgram(shaderID);
}

void Shader::bind() {
    glUseProgram(shaderID);
}

void Shader::setUniform(string uName, GLfloat v0) {
    glUniform1f(getUniformLocation(uName), v0);
}

void Shader::setUniform(string uName, GLfloat v0, GLfloat v1) {
    glUniform2f(getUniformLocation(uName), v0, v1);
}

void Shader::setUniform(string uName, GLfloat v0, GLfloat v1, GLfloat v2) {
    glUniform3f(getUniformLocation(uName), v0, v1, v2);
}

void Shader::setUniform(string uName, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3) {
    glUniform4f(getUniformLocation(uName), v0, v1, v2, v3);
}

void Shader::setUniform(string uName, GLint v0) {
    glUniform1i(getUniformLocation(uName), v0);
}

void Shader::setUniform(string uName, GLuint v0) {
    glUniform1ui(getUniformLocation(uName), v0);
}

void Shader::setUniform(string uName, glm::vec2 v) {
    setUniform(uName, v.x, v.y);
}

void Shader::setUniform(string uName, glm::vec3 v) {
    setUniform(uName, v.x, v.y, v.z);
}

void Shader::setUniform(string uName, glm::vec4 v) {
    setUniform(uName, v.x, v.y, v.z, v.w);
}

void Shader::setUniform(string uName, glm::mat4 v) {
    glUniformMatrix4fv(getUniformLocation(uName), 1, GL_FALSE, &v[0][0]);
}

void setUniform(string uName, Material m) {
    // TODO
}

GLuint Shader::getUniformLocation(string uName) {
    map<string,GLuint>::iterator i = uniforms.find(uName);
    if (i != uniforms.end())
        return i->second;
    else 
        return uniforms[uName] = glGetUniformLocation(shaderID, uName.c_str());
}

GLuint Shader::loadShaders(string vertexFilePath, string geometryFilePath, string fragmentFilePath) {
    GLuint vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
    GLuint fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
    GLuint geometryShaderID;
    if (geometryFilePath != "null") {
        geometryShaderID = glCreateShader(GL_GEOMETRY_SHADER);
    }

    string vertexShaderCode; 
    ifstream vertexShaderStream(vertexFilePath);
     
    if (vertexShaderStream.is_open()) {
        string line = "";
        while (getline(vertexShaderStream, line))
            vertexShaderCode += "\n" + line;
        vertexShaderStream.close();
    } else {
        cerr << "Impossible to open " << vertexFilePath << endl;
        return 0;
    }

    string fragmentShaderCode;
    ifstream fragmentShaderStream(fragmentFilePath);

    if (fragmentShaderStream.is_open()) {
        string line = "";
        while (getline(fragmentShaderStream, line))
            fragmentShaderCode += "\n" + line;
        fragmentShaderStream.close();
    } else {
        cerr << "Impossible to open " << fragmentFilePath << endl;
        return 0;
    }

    string geometryShaderCode;

    if (geometryFilePath != "null") {
        ifstream geometryShaderStream(geometryFilePath);

        if (geometryShaderStream.is_open()) {
            string line = "";
            while (getline(geometryShaderStream, line))
                geometryShaderCode += "\n" + line;
            geometryShaderStream.close();
        } else {
            cerr << "Impossible to open " << geometryFilePath << endl;
            return 0;
        }
    }
    
    GLint result = GL_FALSE;
    int infoLogLength;

    const char* vertexSourcePointer = vertexShaderCode.c_str();
    glShaderSource(vertexShaderID, 1, &vertexSourcePointer , NULL);
    glCompileShader(vertexShaderID);

    glGetShaderiv(vertexShaderID, GL_COMPILE_STATUS, &result);
    glGetShaderiv(vertexShaderID, GL_INFO_LOG_LENGTH, &infoLogLength);

    if (!result) {
        vector<char> vertexShaderErrorMessage(infoLogLength+1);
        glGetShaderInfoLog(vertexShaderID, infoLogLength, NULL, &vertexShaderErrorMessage[0]);
        cerr << &vertexShaderErrorMessage[0] << endl;
    }

    const char* fragmentSourcePointer = fragmentShaderCode.c_str();
    glShaderSource(fragmentShaderID, 1, &fragmentSourcePointer , NULL);
    glCompileShader(fragmentShaderID);

    glGetShaderiv(fragmentShaderID, GL_COMPILE_STATUS, &result);
    glGetShaderiv(fragmentShaderID, GL_INFO_LOG_LENGTH, &infoLogLength);
 
    if (!result) {
        vector<char> fragmentShaderErrorMessage(infoLogLength+1);
        glGetShaderInfoLog(fragmentShaderID, infoLogLength, NULL, &fragmentShaderErrorMessage[0]);
        cerr << &fragmentShaderErrorMessage[0] << endl;
    }

    if (geometryFilePath != "null") {
        const char* geometrySourcePointer = geometryShaderCode.c_str();
        glShaderSource(geometryShaderID, 1, &geometrySourcePointer , NULL);
        glCompileShader(geometryShaderID);

        glGetShaderiv(geometryShaderID, GL_COMPILE_STATUS, &result);
        glGetShaderiv(geometryShaderID, GL_INFO_LOG_LENGTH, &infoLogLength);

        if (!result) {
            vector<char> geometryShaderErrorMessage(infoLogLength+1);
            glGetShaderInfoLog(geometryShaderID, infoLogLength, NULL, &geometryShaderErrorMessage[0]);
            cerr << &geometryShaderErrorMessage[0] << endl;
        }
    }
    
    GLuint programID = glCreateProgram();
    glAttachShader(programID, vertexShaderID);
    if (geometryFilePath != "null") glAttachShader(programID, geometryShaderID); 
    glAttachShader(programID, fragmentShaderID);
    glLinkProgram(programID);

    glGetProgramiv(programID, GL_LINK_STATUS, &result);
    glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &infoLogLength);

    if (!result) {
        vector<char> programErrorMessage(infoLogLength+1);
        glGetProgramInfoLog(programID, infoLogLength, NULL, &programErrorMessage[0]);
        cerr << &programErrorMessage[0] << endl;
    }

    glDetachShader(programID, vertexShaderID);
    glDetachShader(programID, fragmentShaderID);
    if (geometryFilePath != "null") glDetachShader(programID, geometryShaderID);
    
    glDeleteShader(vertexShaderID);
    if (geometryFilePath != "null") glDeleteShader(geometryShaderID);
    glDeleteShader(fragmentShaderID);
    
    return programID;
}

