#include "Material.hpp"

Material::Material() 
    : ambient         { 1.0f, 1.0f, 1.0f }
    , diffuse         { 1.0f, 1.0f, 1.0f }
    , specular        { 1.0f, 1.0f, 1.0f }
    , shininess       { 50.0f } 
    , diffuseTexture  { Texture::nullTexture() } 
    , specularTexture { Texture::nullTexture() }
    , normalTexture   { Texture::flatNormalTexture() } { } 