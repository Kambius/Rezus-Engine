#include "Engine.hpp"
#include "Window.hpp"
#include "Input.hpp"

bool    Engine::running;
Window* Engine::window;
Input*  Engine::input;
Func    Engine::update; 
Func    Engine::draw; 

void Engine::init() { 
    window = new Window("Visualizer", 800, 600);
    input = window->getInput();
}

void Engine::start() {
    mainLoop();
    terminate();
}

void Engine::stop() {
    running = false;
}

void Engine::terminate() {
    window->~Window();
}

void Engine::mainLoop() {
    running = true;

    while (running && !window->shouldClose()) {
        window->updateInput();

        update();
        draw();

        window->swapBuffers();
    }

    running = false;
}

void Engine::setUpdate(Func update) {
    Engine::update = update;
}

void Engine::setDraw(Func draw) {
    Engine::draw = draw;
}

Window& Engine::getWindow() {
    return *window;
}

Input& Engine::getInput() {
    return *input;
}