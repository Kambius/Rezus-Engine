#include "Texture.hpp"
#include <GL/glew.h>
#include <iostream>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

using namespace std;

Texture::Texture(std::string filename) : path{ filename } {
  
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_2D, textureID);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    int comp;
    stbi_set_flip_vertically_on_load(true);
    unsigned char* image = stbi_load(filename.c_str(), &width, &height, &comp, 0);
    if (comp == 3)
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
    else if(comp == 4)
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);    

    glGenerateMipmap(GL_TEXTURE_2D);    
    
    stbi_image_free(image);
    glBindTexture(GL_TEXTURE_2D, 0);
}

Texture::Texture(int width, int height, GLint internalFormat, GLint externalFormat, GLint type, void* data) {
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_2D, textureID);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, externalFormat, type, data);    

    //glGenerateMipmap(GL_TEXTURE_2D);    
    
    glBindTexture(GL_TEXTURE_2D, 0);
}

GLuint Texture::getID() {
    return textureID;   
}

Texture::Texture() : textureID{ 0 } { }

void Texture::dispose() {
    glDeleteTextures(1, &textureID);
}

void Texture::bind() {
    glBindTexture(GL_TEXTURE_2D, textureID);
}

int Texture::getWidth() {
    return width;
}

int Texture::getHeight() {
    return height;
}

std::string Texture::getPath() {
    return path;
}

Texture Texture::nTexture;

Texture* Texture::nullTexture() {
    if (!nTexture.textureID) {
        glGenTextures(1, &nTexture.textureID);
        glBindTexture(GL_TEXTURE_2D, nTexture.textureID);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

        unsigned char data[3] {255, 255, 255};
        glTexImage2D(
            GL_TEXTURE_2D,
            0, GL_RGB, 1, 1, 0, 
            GL_RGB, GL_UNSIGNED_BYTE,
            data
        );

        glBindTexture(GL_TEXTURE_2D, 0);           
    }

    return &nTexture;
}

Texture Texture::fNormalTexture;

Texture* Texture::flatNormalTexture() {
    if (!fNormalTexture.textureID) {
        glGenTextures(1, &fNormalTexture.textureID);
        glBindTexture(GL_TEXTURE_2D, fNormalTexture.textureID);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

        unsigned char data[3] {128, 128, 255};
        glTexImage2D(
            GL_TEXTURE_2D,
            0, GL_RGB, 1, 1, 0, 
            GL_RGB, GL_UNSIGNED_BYTE,
            data
        );

        glBindTexture(GL_TEXTURE_2D, 0);           
    }

    return &fNormalTexture;
}

void Texture::setParameter(GLint param, GLint value) {
    bind();
    glTexParameteri(GL_TEXTURE_2D, param, value);
}

void Texture::setBorderColor(GLfloat r, GLfloat g, GLfloat b, GLfloat a) {
    bind();
    GLfloat borderColor[] = { r, g, b, a };
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
}
