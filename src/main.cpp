#include <Python.h>
#include <iostream>

#include "python.hpp"

using namespace std;
using namespace rz::python;

int pymain() {
    init();
  
    PyObj module      = import("rzpy.rezus");

    PyObj baseClass   = module.atr("RzObject");
    PyObj playerClass = module.atr("Player");
    
    if (!playerClass.isSubclassOf(baseClass)) {
        cerr << "Player is not subclass of RzObject!" << endl;
        return 1;
    }

    PyObj obj         = playerClass.call();
    
    if (!obj.isInstanceOf(baseClass)) {
        cerr << "Object is not instance of RzObject!" << endl;
        return 1;
    }    

    PyObj value       = obj.method("init");

    for (int i = 0; i < 10; ++i) {
        value = obj.method("update");                    
        cout << "Res: " << convert<double>(value) << endl;
    }                   

    rz::python::finalize();

    return 0;
}