#include "CubeMap.hpp"
#include <GL/glew.h>
#include <iostream>

using namespace std;

CubeMap::CubeMap(int width, int height, GLint internalFormat, GLint externalFormat, GLint type, void* data) {
    glGenTextures(1, &cubeMapID);
    glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMapID);
    
    for (GLuint i = 0; i < 6; ++i) {
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, internalFormat, width, height, 0, externalFormat, type, data); 
    }
        
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);  

}


GLuint CubeMap::getID() {
    return cubeMapID;   
}

void CubeMap::dispose() {
    glDeleteTextures(1, &cubeMapID);
}

void CubeMap::bind() {
    glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMapID);
}

void CubeMap::setParameter(GLint param, GLint value) {
    bind();
    glTexParameteri(GL_TEXTURE_CUBE_MAP, param, value);
}
