#include "Transform.hpp"

Transform::Transform() : tr{0,0,0}, rt{1, 0, 0, 0}, sc{1,1,1} { }

glm::mat4 Transform::getMatrix() {
    return glm::translate(tr) * glm::toMat4(rt) * glm::scale(sc); 
}

Transform& Transform::translate(glm::vec3 t) {
    tr += t;
    return *this;
}

Transform& Transform::scale(glm::vec3 s) {
    sc *= s;
    return *this;
}

Transform& Transform::scale(float s) {
    sc *= s;
    return *this;
}

Transform& Transform::rotate(float angle, glm::vec3 axis) {
    rt = glm::rotate(rt, glm::radians(angle), glm::normalize(axis));
    return *this;
}

Transform& Transform::setTranslation(glm::vec3 t) {
    tr = t;
    return *this;
}

Transform& Transform::setScale(glm::vec3 s) {
    sc = s;
    return *this;
}

Transform& Transform::setScale(float s) {
    sc = glm::vec3{s, s, s};
    return *this;
}

Transform& Transform::setRotation(float angle, glm::vec3 axis) {
    rt = glm::angleAxis(glm::radians(angle), glm::normalize(axis));
}

glm::vec3 Transform::getTranslation() {
    return tr;
}

glm::vec3 Transform::getScale() {
    return sc;
}

glm::quat Transform::getRotation() {
    return rt;
}

glm::vec3 Transform::getRotationAxis() {
    return glm::axis(rt);
}

float Transform::getRotationAngle() {
    return glm::degrees(glm::angle(rt));
}
