#include "DepthBuffer.hpp"
#include <glm/glm.hpp>
#include <iostream>

using namespace std;

DepthBuffer::DepthBuffer(int width, int height)
    : width  { width }
    , height { height } {
      
    depthTexture = new Texture(width, height, GL_DEPTH_COMPONENT, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    depthTexture-> setParameter(GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    depthTexture-> setParameter(GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    depthTexture-> setParameter(GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    depthTexture-> setParameter(GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    depthTexture-> setBorderColor(1.0, 1.0, 1.0, 1.0);

    
    glGenFramebuffers(1, &depthBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, depthBuffer);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthTexture->getID(), 0);
    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);
    glBindFramebuffer(GL_FRAMEBUFFER, 0); 
   
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        cout << "ERROR::DEPTHBUFFER:: DepthBuffer is not complete!" << endl;
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void DepthBuffer::bind() {
    glBindFramebuffer(GL_FRAMEBUFFER, depthBuffer);
}

int DepthBuffer::getWidth() {
    return width;
}

int DepthBuffer::getHeight() {
    return height;
}

Texture* DepthBuffer::getDepthTexture() {
    return depthTexture;
}
