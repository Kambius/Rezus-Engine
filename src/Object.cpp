#include "Object.hpp"

using namespace std;

Object::Object() : transform { new Transform { } } { }

Object::Object(string filename) : transform { new Transform { } } {
    Model m { filename };
    meshes = m.meshes;
    materials = m.materials;
}

