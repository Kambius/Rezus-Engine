#include "GBuffer.hpp"

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <iostream>

using namespace std;

GBuffer::GBuffer(int width, int height)
    : width  { width }
    , height { height }  {
      
    glGenFramebuffers(1, &gBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, gBuffer);

    gPosition = new Texture(width, height, GL_RGBA16F, GL_RGBA, GL_FLOAT, NULL);
    gPosition-> setParameter(GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    gPosition-> setParameter(GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    gPosition-> setParameter(GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    gPosition-> setParameter(GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, gPosition->getID(), 0);
    
    gNormal   = new Texture(width, height, GL_RGB16F, GL_RGB, GL_FLOAT, NULL);
    gNormal  -> setParameter(GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    gNormal  -> setParameter(GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    gNormal  -> setParameter(GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    gNormal  -> setParameter(GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, gNormal->getID(), 0);

    gAlbedo   = new Texture(width, height, GL_RGB16, GL_RGB, GL_UNSIGNED_BYTE, NULL);
    gAlbedo  -> setParameter(GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    gAlbedo  -> setParameter(GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    gAlbedo  -> setParameter(GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    gAlbedo  -> setParameter(GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, gAlbedo->getID(), 0);

    gSpec     = new Texture(width, height, GL_RGB16, GL_RGB, GL_UNSIGNED_BYTE, NULL);
    gSpec    -> setParameter(GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    gSpec    -> setParameter(GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    gSpec    -> setParameter(GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    gSpec    -> setParameter(GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_2D, gSpec->getID(), 0);

    /*    
    // - Position color buffer
    glGenTextures(1, &gPosition);
    glBindTexture(GL_TEXTURE_2D, gPosition);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, gPosition, 0);
    
    // - Normal color buffer
    glGenTextures(1, &gNormal);
    glBindTexture(GL_TEXTURE_2D, gNormal);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); 
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, gNormal, 0);
  
    // - Color + Specular color buffer
    glGenTextures(1, &gAlbedo);
    glBindTexture(GL_TEXTURE_2D, gAlbedo);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, gAlbedo, 0);

    glGenTextures(1, &gSpec);
    glBindTexture(GL_TEXTURE_2D, gSpec);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_2D, gSpec, 0);
    */
    
    GLuint rbo;
    glGenRenderbuffers(1, &rbo);
    glBindRenderbuffer(GL_RENDERBUFFER, rbo); 
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);  
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo);
    
    GLuint attachments[4] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3 };
    glDrawBuffers(4, attachments);

    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        cout << "ERROR::FRAMEBUFFER:: Framebuffer is not complete!" << endl;
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void GBuffer::bind() {
    glBindFramebuffer(GL_FRAMEBUFFER, gBuffer);
}

int GBuffer::getWidth() {
    return width;
}

int GBuffer::getHeight() {
    return height;
}

Texture* GBuffer::getPositionTexture() {
    return gPosition;
}

Texture* GBuffer::getNormalTexture() {
    return gNormal;
}

Texture* GBuffer::getAlbedoTexture() {
    return gAlbedo;
}

Texture* GBuffer::getSpecTexture() {
    return gSpec;
}
