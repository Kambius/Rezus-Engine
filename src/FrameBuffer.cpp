#include "FrameBuffer.hpp"

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <iostream>

using namespace std;

FrameBuffer::FrameBuffer(int width, int height)
    : width{ width }
    , height{ height } {

    glGenFramebuffers(1, &framebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
    
    colorTexture = new Texture(width, height, GL_RGB16F, GL_RGB, GL_FLOAT, NULL);
    colorTexture-> setParameter(GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    colorTexture-> setParameter(GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    colorTexture-> setParameter(GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    colorTexture-> setParameter(GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    /*
    glGenTextures(1, &colorTexture);
    glBindTexture(GL_TEXTURE_2D, colorTexture);

    glTexImage2D(
        GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 
        0, GL_RGB, GL_FLOAT, NULL
    );

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glBindTexture(GL_TEXTURE_2D, 0);
    */
    
    glFramebufferTexture2D(
        GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, 
        GL_TEXTURE_2D, colorTexture->getID(), 0
    ); 

    GLuint rbo;
    glGenRenderbuffers(1, &rbo);
    glBindRenderbuffer(GL_RENDERBUFFER, rbo); 
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);  
    glBindRenderbuffer(GL_RENDERBUFFER, 0);

    glFramebufferRenderbuffer(
        GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, 
        GL_RENDERBUFFER, rbo
    );

    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        cout << "ERROR::FRAMEBUFFER:: Framebuffer is not complete!" << endl;
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void FrameBuffer::bind() {
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
}

int FrameBuffer::getWidth() {
    return width;
}

int FrameBuffer::getHeight() {
    return height;
}

Texture* FrameBuffer::getColorTexture() {
    return colorTexture;
}
