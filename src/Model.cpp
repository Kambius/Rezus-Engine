#include "Model.hpp"
#include <iostream>

using namespace std;

Model::Model(string path) {
    loadModel(path);
}

void Model::loadModel(string path) {
    Assimp::Importer importer;
    const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_CalcTangentSpace/* | aiProcess_FlipUVs*/);

    if (!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) {
        cout << "ERROR::ASSIMP:: " << importer.GetErrorString() << endl;
        return;
    }
    
    directory = path.substr(0, path.find_last_of('/'));
    if (directory == path)
        directory = ".";
    processNode(scene->mRootNode, scene);
}

void Model::drawAllMeshes() {
    for (int i = 0; i < meshes.size(); ++i) {
        meshes[i]->draw();
    }
}


void Model::processNode(aiNode* node, const aiScene* scene) {
    for (GLuint i = 0; i < node->mNumMeshes; i++) {
        aiMesh* mesh = scene->mMeshes[node->mMeshes[i]]; 
        processMesh(mesh, scene);       
    }
 
    for (GLuint i = 0; i < node->mNumChildren; i++) 
        processNode(node->mChildren[i], scene);
}

void Model::processMesh(aiMesh* mesh, const aiScene* scene) {
    // Data to fill
    vector<Vertex> vertices;
    vector<GLuint> indices;
    vector<Texture*> textures;

    // Walk through each of the mesh's vertices
    for (GLuint i = 0; i < mesh->mNumVertices; i++) {
        Vertex vertex;
        glm::vec3 vector; // We declare a placeholder vector since assimp uses its own vector class that doesn't directly convert to glm's vec3 class so we transfer the data to this placeholder glm::vec3 first.

        // Positions
        vector.x = mesh->mVertices[i].x;
        vector.y = mesh->mVertices[i].y;
        vector.z = mesh->mVertices[i].z;
        vertex.position = vector;

        // Normals
        vector.x = mesh->mNormals[i].x;
        vector.y = mesh->mNormals[i].y;
        vector.z = mesh->mNormals[i].z;
        vertex.normal = vector;

        if (mesh->mTextureCoords[0]) {
            // Tangents
            vector.x = mesh->mTangents[i].x;
            vector.y = mesh->mTangents[i].y;
            vector.z = mesh->mTangents[i].z;
            vertex.tangent = vector;

            // Bitangents
            vector.x = mesh->mBitangents[i].x;
            vector.y = mesh->mBitangents[i].y;
            vector.z = mesh->mBitangents[i].z;
            vertex.bitangent = vector;
        } else {
            vector = glm::cross(vertex.normal, glm::vec3{1, 0, 0});
            if (vector == glm::vec3{0, 0, 0}) glm::cross(vertex.normal, glm::vec3{0, 1, 0}); 
            vertex.tangent = vector;
            vertex.bitangent = glm::cross(vertex.normal, vertex.tangent);
        }

        // Texture Coordinates
        if (mesh->mTextureCoords[0]) {
            glm::vec2 vec;
            // A vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't 
            // use models where a vertex can have multiple texture coordinates so we always take the first set (0).
            vec.x = mesh->mTextureCoords[0][i].x; 
            vec.y = mesh->mTextureCoords[0][i].y;
            vertex.texCoords = vec;
        } else {
            vertex.texCoords = glm::vec2(0.0f, 0.0f);
        }
        
        vertices.push_back(vertex);
    }
    
    // Now wak through each of the mesh's faces (a face is a mesh its triangle) and retrieve the corresponding vertex indices.
    for (GLuint i = 0; i < mesh->mNumFaces; i++) {
        aiFace face = mesh->mFaces[i];
        // Retrieve all indices of the face and store them in the indices vector
        for (GLuint j = 0; j < face.mNumIndices; j++)
            indices.push_back(face.mIndices[j]);
    }

    meshes.push_back(new Mesh(vertices, indices));

    // Process materials
    aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];


    Material* m = new Material{};
    // 1. Diffuse maps
    vector<Texture*> diffuseMaps = loadMaterialTextures(material, aiTextureType_DIFFUSE);
   
    if (diffuseMaps.size() > 0)
        m->diffuseTexture = diffuseMaps[0];
    // 2. Specular maps
    vector<Texture*> specularMaps = loadMaterialTextures(material, aiTextureType_SPECULAR);
    if (specularMaps.size() > 0)
        m->specularTexture = specularMaps[0];

    vector<Texture*> normalMaps = loadMaterialTextures(material, aiTextureType_HEIGHT);
    if (normalMaps.size() > 0)
        m->normalTexture = normalMaps[0];

    materials.push_back(m);
}

vector<Texture*> Model::loadMaterialTextures(aiMaterial* mat, aiTextureType type) {
    vector<Texture*> textures;
    
    for (GLuint i = 0; i < mat->GetTextureCount(type); i++) {
        aiString str;
        mat->GetTexture(type, i, &str);
        string filename = directory + '/' + str.C_Str();

        Texture* texture = new Texture(filename);
        textures.push_back(texture);
    }
    
    return textures;
}

void Model::draw(Shader* shader) {
    for (GLuint i = 0; i < meshes.size(); ++i) {
        shader->setUniform("material.ambient", materials[i]->ambient);
        shader->setUniform("material.diffuse", materials[i]->diffuse);
        shader->setUniform("material.specular", materials[i]->specular);

        glActiveTexture(GL_TEXTURE0);
        shader->setUniform("material.diffuseTexture", 0);
        materials[i]->diffuseTexture->bind();
        glActiveTexture(GL_TEXTURE1);
        shader->setUniform("material.specularTexture", 1);
        materials[i]->specularTexture->bind();
        glActiveTexture(GL_TEXTURE2);
        shader->setUniform("material.normalTexture", 2);
        materials[i]->normalTexture->bind();
        
        meshes[i]->draw();
    }
}  

